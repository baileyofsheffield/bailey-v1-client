const path = require('path');
const cookieParser = require('cookie-parser');
const env = process.env.NODE_ENV || 'development';
const ROOT = path.join(path.resolve(__dirname, '..'));
const compression = require('compression');
const httpProxy = require('http-proxy');
const targetUrl = 'http://localhost:3000/api';
const exphbs  = require('express-handlebars');
const proxy = httpProxy.createProxyServer({
    target: targetUrl,
    changeOrigin: true
});

module.exports = function(app, express) {
    app.set('env', env);
    app.set('port', process.env.PORT || 9999);
    app.engine('handlebars', exphbs());
    app.set('views', path.join(__dirname, '../dist'));
    app.set('view engine', 'handlebars');

    app.enable('trust proxy');
    app.disable('x-powered-by');
    app.use(cookieParser('90c03ba15e17a53908a4a57ff110f03e'));
    app.use(compression());
    // Serve static files
    app.use('/.well-known', express.static(process.cwd() + '/.well-known'));

    app.use('/assets', express.static(path.join(__dirname, '../assets'), {
        maxAge: 30
    }));

    app.use('/img', express.static(path.join(__dirname, '../assets/img'), {
        maxAge: 30
    }));

    app.use('/svg', express.static(path.join(__dirname, '../assets/svg'), {
        maxAge: 30
    }));

    app.use('/fonts', express.static(path.join(__dirname, '../assets/fonts'), {
        maxAge: 30
    }));

    app.use('/video', express.static(path.join(__dirname, '../assets/video'), {
        maxAge: 30
    }));

    app.use('/api', (req, res) => {
        proxy.web(req, res, {
            target: targetUrl
        });
    });

    function cacheControl(req, res, next) {
        // instruct browser to revalidate in 60 seconds
        res.header('Cache-Control', 'max-age=60');
        next();
    }

    app.use(cacheControl, express.static(path.join(__dirname, '../dist'), {
        maxAge: 30
    }));

    app.get('/*', function(req, res) {
        res.render('index', {
            environment: process.env.NODE_ENV,
            trackingCode: process.env.GA_CODE,
            stripeKey: process.env.STRIPE_KEY,
            tagManager: process.env.TAG_MANAGER
        });
    });
};
