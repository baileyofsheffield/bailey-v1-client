var path = require('path');
var Webpack = require('webpack');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
    entry: {
        vendor: ['underscore', 'jquery', 'backbone', 'backbone.marionette', 'modernizer', 'notificationFx'],
        frontend: [__dirname + '/src/main']
    },
    output: {
        path: __dirname + '/dist',
        filename: "js/[name].js"
    },
    resolve: {
        alias: {
            modernizer: './assets/js/lib/modernizer.js',
            notificationFx: './assets/js/lib/notificationFx.js'
        }
    },
    devtool: "source-map",
    plugins: [
        new Webpack.optimize.CommonsChunkPlugin({
            name: 'vendor',
            minChunks: Infinity,
            filename: 'js/[name].[chunkhash:8].js'
        }),
        new HtmlWebpackPlugin({
            filename: __dirname + '/dist/index.handlebars',
            template: __dirname + '/src/index.handlebars'
        }),
        // extract inline css into separate 'styles.css'
        new ExtractTextPlugin({
            filename: "css/[name].css",
            disable: false,
            allChunks: true
        }),
        new Webpack.ProvidePlugin({
            _: 'underscore',
            $: 'jquery',
            jQuery: 'jquery',
            "window.jQuery": "jquery",
            Backbone: 'backbone',
            Bb: 'backbone',
            Marionette: 'backbone.marionette',
            Mn: 'backbone.marionette',
        }),
        // new Webpack.optimize.UglifyJsPlugin({
        //     minimize: true,
        //     compress: {
        //         warnings: false,
        //         conditionals: true,
        //         unused: true,
        //         comparisons: true,
        //         sequences: true,
        //         dead_code: true,
        //         evaluate: true,
        //         if_return: true,
        //         join_vars: true,
        //         negate_iife: false // we need this for lazy v8
        //     },
        //     mangle: {
        //         except: ['$super', '$', 'exports', 'require']
        //     },
        //     output: {
        //         comments: false
        //     },
        //     'screw-ie8': true,
        //     sourceMap: true
        // })
    ],
    module: {
        loaders: [{
                test: /\.hbs$/,
                loader: 'handlebars-template-loader'
            },
            {
                test: /\.less$/,
                loader: ExtractTextPlugin.extract(['css-loader', 'less-loader'])
            },
            {
                test: /\.(woff|woff2)$/,
                loader: "url?limit=10000&mimetype=application/font-woff"
            },
            {
                test: /\.ttf$/,
                loader: "url?limit=10000&mimetype=application/octet-stream"
            },
            {
                test: /\.eot$/,
                loader: "file"
            },
            {
                test: /\.svg$/,
                loader: "url?limit=10000&mimetype=image/svg+xml"
            }
        ]
    }
};
