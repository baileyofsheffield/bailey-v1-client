require('dotenv').config({ silent: true });

const express = require('express');
const app = express();

require('./config/express')(app, express);

app.listen(app.get('port'), function() {
  console.log("\n✔ Express server listening on port %d in %s mode", app.get('port'), app.get('env'));
});

module.exports = app;
