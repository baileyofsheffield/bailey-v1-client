  var Cookies = require('cookies-js'),
  	message = require('./config/message'),
  	userChannel = require('./config/userChannel'),
  	globalChannel = require('./config/globalChannel'),
  	ForbibbenView = require('./common/status/forbidden'),
  	NotFoundView = require('./common/status/notFound');

  require('backbone-validation');
  require('./config/regions/modal');
  Backbone.Syphon = require('backbone.syphon');
  Backbone.Select = require('backbone.select');
  _.extend(Backbone.Model.prototype, Backbone.Validation.mixin);

  var body = document.querySelector('body');

  function menuBar(e) {
  	var route = app.getCurrentRoute();
  	if (route === 'shop/custom') return;
  	if (window.pageYOffset > 60) {
  		body.classList.add('bar-menu');
  	}

  	if (window.pageYOffset < 60) {
  		body.classList.remove('bar-menu');
  	}
  }

  window.addEventListener('scroll', menuBar, false);

  var app = new Mn.Application({
  	regions: {
  		header: ".header",
  		account: ".account",
  		cart: ".cart",
  		menu: ".main-menu",
  		mobileMenu: ".mobile-menu",
  		main: ".main",
  		footer: ".footer",
  		footerMainMenu: ".f-main",
  		footerYourMenu: ".f-your",
  		footerTechMenu: ".f-tech",
  		footerPolicyMenu: ".f-policy",
  		modalRegion: Mn.Region.Modal.extend({
  			el: "#modal"
  		})
  	},
  	navigate: function(route, options) {
  		globalChannel.trigger("set:active:header", "none");
  		if (options === null) {
  			options = {};
  		}
  		$('body').removeClass();
  		if (route == 'login' || route == 'checkout/login' || route == 'reset') {
  			$('body').addClass('bg-grey');
  		}
  		Backbone.history.navigate(route, options);
  		$(window).scrollTop(0);
  	},
  	getCurrentRoute: function() {
  		return Backbone.history.fragment;
  	},
  	startSubApp: function(appName) {
  		var currentApp = appName ? app.module(appName) : null;
  		if (app.currentApp === currentApp) {
  			return;
  		}
  		if (app.currentApp) {
  			app.currentApp.stop();
  		}
  		app.currentApp = currentApp;
  		if (currentApp) {
  			currentApp.start();
  		}
  	}
  });

  window.onpopstate = function(e) {
  	$('body').removeClass();
  	var scrollTop;
  	if (history.state && history.state.scrollTop) {
  		scrollTop = history.state.scrollTop;
  	} else {
  		scrollTop = 0;
  	}
  	$(window).scrollTop(scrollTop);
  };

  Backbone.ajax = function() {
  	Backbone.$.ajaxSetup.call(Backbone.$, {
  		statusCode: {
  			401: function() {
  				globalChannel.trigger('login:show');
  			},
  			403: function(resp) {
  				var user = userChannel.request('current:user');
  				var authed = user.get('auth');
  				if (authed) {
  					app.main.show(new ForbibbenView());
  				} else if (app.getCurrentRoute() !== 'login' && app.getCurrentRoute() !== 'checkout/login') {
  					globalChannel.trigger('login:show');
  				}
  			},
  			404: function() {
  				app.main.show(new NotFoundView());
  			},
  			500: function(resp) {
          Rollbar.error(resp);
  				message("error", "Sorry an error has occured, please try again. If the problem persists, please contact hello@baileyofsheffield.com.");
  			}
  		}
  	});
  	return Backbone.$.ajax.apply(Backbone.$, arguments);
  };

  app.on('start', function() {
  	var user = userChannel.request('current:user');

  	var popup_timer = setTimeout(showPopup, 60000);

  	if (Cookies.get('viewedOuibounceModal') || user.get('mailchimp')) {
  		clearTimeout(popup_timer);
  	}

  	function showPopup() {
  		$('#ouibounce-modal').show().addClass('show');
  		var popup_expire = 60 * 60 * 24 * 15;
  		Cookies.set('viewedOuibounceModal', true, {
  			expires: popup_expire
  		});
  		clearTimeout(popup_timer);
  	}

  	$('.close-model').on('click', function() {
  		$('#ouibounce-modal').hide().removeClass('show');
  	});

  	$('#mc-embedded-subscribe').on('click', function() {
  		$('#ouibounce-modal').hide().removeClass('show');
  	});
  });

  module.exports = app;
