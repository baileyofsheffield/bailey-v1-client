var Handlebars = require('handlebars-template-loader/runtime'),
  tpl = require('./templates/ordering.hbs');
Handlebars.registerPartial("ordering", tpl);
