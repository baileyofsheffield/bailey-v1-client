var Handlebars = require('handlebars-template-loader/runtime'),
  tpl = require('./templates/traceable.hbs');
Handlebars.registerPartial("traceable", tpl);
