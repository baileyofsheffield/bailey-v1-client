var Validation = require('backbone-validation');

module.exports = Mn.Behavior.extend({
  onRender: function() {
    //Set up any other form related stuff here
    Backbone.Validation.bind(this.view, {
      valid: function(view, attr, selector) {
        var control, group;
        attr = attr.split(".").join("");
        control = view.$('[id~=' + attr + ']');
        group = control.parents('.form-group');
        group.removeClass('has-error');
        group.find('.help-block.error-message').remove();
      },
      invalid: function(view, attr, error, selector) {
        attr = attr.split(".").join("");
        console.log(attr);
        console.log(error);
        var control, group, position, target;
        control = view.$('[id~=' + attr + ']');
        group = control.parents('.form-group');
        group.addClass('has-error');
        if (group.find('.help-block').length === 0) {
          group.find('.form-control:visible').after('<p class=\'help-block error-message\'></p>');
        }
        target = group.find('.help-block');
        target.text(error);
      }
    });
  }

});
