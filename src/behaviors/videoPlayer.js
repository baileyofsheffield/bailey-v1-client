module.exports = Mn.Behavior.extend({
  events: {
    'click video': 'playVideo',
    'click .video-play': 'playVideo'
  },
  onShow: function () {
    $('#video').prop("controls", false);
  },
  playVideo: function (e) {
    e.preventDefault();
    $('.video-play').hide();
    var video = document.getElementById("video");
    video.play();
    $('#video').prop("controls", true);
    $('.video-overlay').hide();
  }
});
