module.exports = Mn.Behavior.extend({
  initialize: function() {
    this.listenTo(this.view, 'loading:stop', this.onStopLoading);
  },
  events: {
    "click a.link": "showLoading",
    "click button.link": "showLoading",
    "click button.add-cart": "showLoading"
  },
  showLoading: function (e) {
    e.preventDefault();
    $(e.currentTarget).addClass('wait');
  },
  onStopLoading: function () {
    $('.wait').removeClass('wait');
  }
});
