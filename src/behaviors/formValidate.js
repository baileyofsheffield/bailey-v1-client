module.exports = Mn.Behavior.extend({
  events: {
    "click button.js-submit": "submitClicked",
    "click button.js-close": "closeClicked",
    "keyup input.pre-check": "preValidate",
    "change input.pre-check": "preValidate"
  },
  initialize: function() {
    this.listenTo(this.view, 'button:reset', this.onButtonReset);
    this.listenTo(this.view, 'validate:field', this.onValidateError);
  },
  onRender: function() {
    Backbone.Validation.bind(this.view, {
      valid: function(view, attr, selector) {
        var control, group;
        attr = attr.split(".").join("");
        control = view.$('[id~=' + attr + ']');
        group = control.parents('.form-group');
        group.removeClass('has-error');
        group.find('.input-error').remove();
        // var btn_title = view.$('.js-submit').data('title');
        // view.$('.js-submit').prop('disabled', false).text(btn_title);
      },
      invalid: function(view, attr, error, selector) {
        attr = attr.split(".").join("");
        var control, group, position, target;
        control = view.$('[id~=' + attr + ']');
        group = control.parents('.form-group');
        group.addClass('has-error');
        if (group.find('.input-error').length === 0) {
          group.find('.form-control:visible').after('<span class="input-error"><svg><use xlink:href="/svg/sprite.svg#info"></use></svg></span>');
        }
        var btn_title = view.$('.js-submit').data('title');
        view.$('.js-submit').prop('disabled', false).text(btn_title);
      }
    });
  },
  onButtonReset: function () {
    var btn_title = this.view.$('.js-submit').data('title');
    this.view.$('.js-submit').prop('disabled', false).text(btn_title);
  },
  preValidate: function(e) {
    e.preventDefault();
    this.onValidateError(e.target);
  },
  onValidateError: function (item) {
    var attr = $(item).attr('name');
    var value = $(item).val();
    var error = this.view.model.preValidate(attr, value);
    var group = $(item).parents('.form-group');
    if (error) {
      group.find('.input-success').remove();
      group.removeClass('has-success');
      group.addClass('has-error');
      if (group.find('.input-error').length === 0) {
        group.find('.form-control:visible').after('<span class="input-error"><svg><use xlink:href="/svg/sprite.svg#info"></use></svg></span>');
      }
    } else {
      group.find('.input-error').remove();
      group.removeClass('has-error');
      group.addClass('has-success');
      if (group.find('.input-success').length === 0) {
        group.find('.form-control:visible').after('<span class="input-success"><svg><use xlink:href="/svg/sprite.svg#tick"></use></svg></span>');
      }
    }
  },
  submitClicked: function(e) {
    e.preventDefault();
    var data = Backbone.Syphon.serialize(this);
    this.view.trigger("form:submit", data);
    this.view.$('.js-submit').prop('disabled', true).text('Please wait...');
  },

  closeClicked: function(e) {
    e.preventDefault();
    this.view.trigger("form:close");
  }
});
