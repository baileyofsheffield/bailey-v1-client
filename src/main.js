require('./setup');
require('./less/styles.less');
var app = require('./app'),
  Cookies = require('cookies-js'),
  HeaderModule = require('./apps/header/app'),
  FooterModule = require('./apps/footer/app'),
  HomeModule = require('./apps/home/app'),
  PageModule = require('./apps/page/app'),
  ShopModule = require('./apps/shop/app'),
  CartModule = require('./apps/cart/app'),
  CheckoutModule = require('./apps/checkout/app'),
  UserModule = require('./apps/user/app'),
  LoginModule = require('./apps/login/app'),
  globalChannel = require('./config/globalChannel'),
  userChannel = require('./config/userChannel'),
  request = require('superagent'),
  Polyglot = require('node-polyglot'),
  langType = 'setup',
  langCookie = false;

require('./entities/currentUser');
require('./entities/currentSettings');
require('./entities/basket');

//Helpers
require('./helpers/helpers');

//Partials
require('./partials/miniLogo');
require('./partials/svgLogo');
require('./partials/afterCare');
require('./partials/ordering');
require('./partials/traceable');
require('./partials/promo');

app.config = {
  cloud: 'https://res.cloudinary.com/dot-to-dot-design/image/upload/',
  production: false
};

var loadInitialData = new Promise(function(resolve, reject) {
  app.i18n = {
    acceptedLanguages: ['uk', 'us', 'es', 'fr', 'de']
  };
  if (Cookies.enabled) {
    app.i18n.currentLanguage = Cookies.get('language') || "uk";
    if (Cookies.get('language')) langType = 'change';
  } else {
    app.i18n.currentLanguage = "uk";
    langType = 'change';
  }
  return request.get('/api/public/session').query({lang: app.i18n.currentLanguage}).then(function(res) {
    userChannel.trigger('set:current:user', res.body.user);
    globalChannel.trigger('set:current:settings', res.body.settings);
    if (res.body.settings.show_offer_banner) {
      $('html').addClass('has-offer-banner');
    }
    if (res.body.user.env_mode == 'production') {
      app.config.production = true;
    }
    if (res.body.user.userid) {
      dataLayer.push({
        'userId': res.body.user.userid
      });
    }
    window.polyglot = new Polyglot();
    _.bindAll(polyglot, "t");
    window.t = polyglot.t;
    polyglot.extend(res.body.locales);
    var options = {
      items: res.body.line_items,
      order: res.body.order
    };
    return resolve(options);
  }).catch(function(err) {
    Rollbar.error(err);
    return reject(err);
  });
});

loadInitialData.then(function(options) {
  app.module('HeaderApp', HeaderModule).start();
  app.module('HomeApp', HomeModule);
  app.module('ShopApp', ShopModule);
  app.module('CartApp', CartModule);
  app.module('CheckoutApp', CheckoutModule);
  app.module('LoginApp', LoginModule);
  app.module('UserApp', UserModule);
  app.module('PageApp', PageModule);
  app.module('FooterApp', FooterModule).start();
  if (Backbone.history) {
    Backbone.history.start({
      pushState: true
    });
    if ('scrollRestoration' in history) {
      history.scrollRestoration = 'manual';
    }
  }
  globalChannel.trigger('basket:add', options.items, options.order);
  app.start();
  return true;
});
