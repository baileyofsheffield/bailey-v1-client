var Handlebars = require('handlebars-template-loader/runtime'),
  app = require('../app'),
  format = require('date-fns/format');

Handlebars.registerHelper("cloudFormat", function(style, id) {
  return app.config.cloud + style + '/' + id;
});

Handlebars.registerHelper("cloudUrl", function(style, id) {
  return app.config.cloud + style + '/bailey/' + id;
});

Handlebars.registerHelper('compare', function(lvalue, rvalue, options) {

  if (arguments.length < 3)
    throw new Error("Handlerbars Helper 'compare' needs 2 parameters");

  var operator = options.hash.operator || "==";

  var operators = {
    '==': function(l, r) {
      return l == r;
    },
    '===': function(l, r) {
      return l === r;
    },
    '!=': function(l, r) {
      return l != r;
    },
    '<': function(l, r) {
      return l < r;
    },
    '>': function(l, r) {
      return l > r;
    },
    '<=': function(l, r) {
      return l <= r;
    },
    '>=': function(l, r) {
      return l >= r;
    },
    'typeof': function(l, r) {
      return typeof l == r;
    }
  };

  if (!operators[operator])
    throw new Error("Handlerbars Helper 'compare' doesn't know the operator " + operator);

  var result = operators[operator](lvalue, rvalue);

  if (result) {
    return options.fn(this);
  } else {
    return options.inverse(this);
  }
});

Handlebars.registerHelper("decimalPrice", function(value) {
  var price = (value / 100).toFixed(2);
  return '£' + price;
});

Handlebars.registerHelper("decimalRange", function(array) {
  var lowest = _.min(array, function(o){return o.price;});
  var highest = _.max(array, function(o){return o.price;});
  if (lowest.price == highest.price) {
    var price = (lowest.price / 100).toFixed(2);
    return '£' + price;
  } else {
    var range = '£' + (lowest.price / 100).toFixed(2) + ' - £' + (highest.price / 100).toFixed(2);
    return range;
  }
});

Handlebars.registerHelper("formatDate", function(date, outputPattern, inputPattern) {
  var defaultPattern = '';
  if (date) {
    if (!outputPattern || (typeof(outputPattern) !== 'string')) {
      outputPattern = defaultPattern;
    }
    return format(date, outputPattern);
  } else {
    return "";
  }
});

Handlebars.registerHelper("i18nUrl", function(url, section) {
  url = Handlebars.escapeExpression(url);
  section = Handlebars.escapeExpression(section);
  if (section) {
    return app.i18n.currentLanguage + "/" + section + "/" + url;
  } else {
    return app.i18n.currentLanguage + "/" + url;
  }
});

Handlebars.registerHelper("ifvalue", function(conditional, options) {
  if (options.hash.value === String(conditional)) {
    return options.fn(this);
  } else {
    return options.inverse(this);
  }
});

Handlebars.registerHelper("imageCdn", function(path) {
  return app.config.cdn + path;
});

Handlebars.registerHelper("langField", function(lang, fields, field) {
  var obj = _.find(fields, function(obj) {
    return obj.lang == lang;
  });
  if (obj) {
    return obj[field];
  } else {
    return;
  }
});

Handlebars.registerHelper("salePrice", function(value, discount) {
  var price = ((value - discount) / 100).toFixed(2);
  return '£' + price;
});

Handlebars.registerHelper('t', function(i18n_key) {
  var result = window.t(i18n_key);
  return new Handlebars.SafeString(result);
});

Handlebars.registerHelper("textArea", function(text) {
  if (text) {
    return new Handlebars.SafeString(text.replace(/<p>/g, '').replace(new RegExp('</p>$'), '').replace(/<br>/g, '\n').replace(/<\/p>/g, '\n\n'));
  } else {
    return;
  }
});
