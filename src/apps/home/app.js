var app = require('../../app'),
  HomeRouter = require('./router'),
  HomeController = require('./controller'),
  HomeModule;

HomeModule = Mn.Module.extend({
  startWithParent: false,
  initialize: function() {
    this.controller = new HomeController();
    this.router = new HomeRouter({
      controller: this.controller
    });
  },
  onStop: function() {
    this.controller.destroy();
  }
});

module.exports = HomeModule;
