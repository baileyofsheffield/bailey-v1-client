var app = require('../../app'),
  HomeControllerBase = require('./controller_actions/base'),
  ShowHome = require('./controller_actions/show_home'),
  globalChannel = require('../../config/globalChannel');

module.exports = HomeControllerBase.extend({
  show: function(lang) {
    app.startSubApp("HomeApp");
    ShowHome(this, app.main);
    globalChannel.trigger("set:active:header", "");
  }
});
