var PageModel = require('../../../entities/pageId'),
  LoadingView = require('../../../common/loading'),
  LayoutView = require('../views/show/layout'),
  FeaturedView = require('../views/show/featured'),
  IntroView = require('../views/show/intro'),
  SectionView = require('../views/show/section'),
  meta = require('../../../config/meta'),
  globalChannel = require('../../../config/globalChannel');

module.exports = function(self, region) {
  region.show(new LoadingView());
  var settings = globalChannel.request('current:settings');
  var page = new PageModel({id: '55d5a31ce547c26872d6d1cd'});

  page.fetch().then(function(){

    var layoutView = new LayoutView({
      model: settings
    });
    var featuredView = new FeaturedView();
    var sectionView = new SectionView();

    var introView = new IntroView({
      model: page
    });

    self.listenTo(layoutView, 'show', function() {
      layoutView.featuredRegion.show(featuredView);
      layoutView.introRegion.show(introView);
      layoutView.mainRegion.show(sectionView);
    });

    self.listenTo(featuredView, 'shop:list', function() {
      layoutView.triggerMethod('destroy');
      globalChannel.trigger("shop:list");
    });

    self.listenTo(introView, 'story:show', function() {
      layoutView.triggerMethod('destroy');
      globalChannel.trigger("story:show");
    });

    region.show(layoutView);
    meta('Home of the stainless steel CABLE™ bracelet', 'Bailey of Sheffield', 'The home of the stainless steel CABLE™ bracelet - Jewellery to last more than a lifetime. Invented, designed, manufactured and hand assembled in Sheffield, UK.');
  });
};
