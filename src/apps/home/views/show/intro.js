var tpl = require('./templates/intro.hbs');

module.exports = Mn.ItemView.extend({
  template: tpl,
  events: {
    'click a.link': 'showStory'
  },
  showStory: function(e) {
    e.preventDefault();
    this.trigger('story:show');
  }
});
