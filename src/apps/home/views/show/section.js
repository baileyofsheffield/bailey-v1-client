var VideoBehavior = require('../../../../behaviors/videoPlayer'),
  tpl = require('./templates/section.hbs');

module.exports = Mn.ItemView.extend({
  className: 'home-links',
  template: tpl,
  behaviors: {
    videoPlayer: {
      behaviorClass: VideoBehavior
    }
  }
});
