var tpl = require('./templates/featured.hbs'),
  owlCarousel = require('owl.carousel');

module.exports = Mn.ItemView.extend({
  template: tpl,
  className: 'gallery-featured',
  triggers: {
    'click .pre-order': 'shop:list'
  },
  events: {
    'click .scroll-icon': 'scrollDown'
  },
  onShow: function() {
    $('.gallery-featured').owlCarousel({
      items: 1,
      loop: true,
      autoplay: true,
      dots: true,
      nav: true,
      nestedItemSelector: 'gallery-cell'
    });
  },
  scrollDown: function (e) {
    e.preventDefault();
    var $target = $('.intro');
    $('html, body').stop().animate({
      'scrollTop': $target.offset().top - 60
    }, 500, 'swing');
  }
});
