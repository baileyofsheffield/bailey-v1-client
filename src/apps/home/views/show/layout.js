var tpl = require('./templates/layout.hbs');
module.exports = Mn.LayoutView.extend({
  tagName: "div",
  className: "home-layout",
  template: tpl,
  regions: {
    featuredRegion: ".featured",
    introRegion: ".intro",
    mainRegion: ".home-content"
  },
  onRender: function() {
    $('body').addClass('home-page');
  }
});
