var OrderModel = require('../../../entities/order'),
  AddressModel = require('../../../entities/address'),
  LineItemCollection = require('../../../entities/lineitems'),
  CountryCollection = require('../../../entities/countries'),
  AddressCollection = require('../../../entities/addresses'),
  LoadingView = require('../../../common/loading'),
  LayoutView = require('../views/show/layout'),
  CartsView = require('../views/show/carts'),
  ShippingView = require('../views/show/shipping'),
  PaymentView = require('../views/show/payment'),
  DiscountView = require('../views/show/discount'),
  GiftView = require('../views/show/gift'),
  ProgressView = require('../views/progress/view'),
  globalChannel = require('../../../config/globalChannel'),
  userChannel = require('../../../config/userChannel'),
  csrf = require('../../../config/csrf'),
  stripeToken = require('../../../config/stripeToken'),
  createPayment = require('../../../config/createPayment'),
  discountCheck = require('../../../config/discountCheck'),
  message = require('../../../config/message'),
  GA = require('../../../config/GA'),
  discounts = false,
  meta = require('../../../config/meta');

module.exports = function(self, region, step) {
  region.show(new LoadingView());
  var order = new OrderModel();
  var user = userChannel.request('current:user');
  var settings = globalChannel.request('current:settings');
  var lineitems = new LineItemCollection();
  var countries = new CountryCollection();
  var addresses = new AddressCollection();
  var addressType, pageTitle;
  switch (step) {
    case 'shipping':
      addressType = 'Shipping';
      pageTitle = "Checkout shipping";
      break;
    case 'payment':
      addressType = 'Billing';
      pageTitle = "Checkout payment";
      break;
  }
  Promise.all([discountCheck.isAvailable(), order.fetch({
    data: {
      checkout: true
    }
  }), lineitems.fetch(), countries.fetch(), addresses.fetch({
    data: {
      type: addressType
    }
  })]).then(function(result) {
    if (result[0].count > 0) {
      discounts = true;
    }
    var has_cable = _.some(lineitems.toJSON(), {
      "product_category": "Cable™"
    });
    var has_christmas = _.some(lineitems.toJSON(), {
      "product_category": "Christmas"
    });
    if (!order.get('id')) return globalChannel.trigger('shop:list');
    if (user.get('auth') || !user.get('auth') && user.get('guest')) {
      var layoutView = new LayoutView();
      var cartsView = new CartsView({
        collection: lineitems,
        model: order
      });

      var stepView, address, stepNo;

      switch (step) {
        case 'shipping':
          stepNo = 2;
          var shippingAddress = order.get('shipping_address');
          if (shippingAddress) {
            address = new AddressModel(shippingAddress);
          } else {
            address = new AddressModel();
          }
          stepView = new ShippingView({
            model: address,
            order: order,
            countries: countries,
            addresses: addresses,
            auth: user.get('auth'),
            has_christmas: has_christmas
          });
          break;
        case 'payment':
          stepNo = 3;
          var billingAddress = order.get('billing_address');
          if (billingAddress) {
            address = new AddressModel(billingAddress);
          } else {
            address = new AddressModel();
          }
          stepView = new PaymentView({
            model: address,
            order: order,
            countries: countries,
            addresses: addresses
          });
          break;
      }

      var progressView = new ProgressView({
        step: step
      });

      var discountView = new DiscountView({
        model: order
      });

      var giftView = new GiftView({
        model: order,
        settings: settings
      });

      self.listenTo(layoutView, 'show', function() {
        layoutView.cartsRegion.show(cartsView);
        layoutView.stepRegion.show(stepView);
        layoutView.progressRegion.show(progressView);
      });
      var special_offer = order.get('special_offer');
      if (stepNo == 2) {
        self.listenTo(stepView, 'show', function() {
          if (discounts && !special_offer && typeof special_offer != 'undefined' && !has_christmas) {
            stepView.discountRegion.show(discountView);
          }
          if (has_cable) {
            stepView.giftRegion.show(giftView);
          }
        });
      }

      self.listenTo(stepView, 'checkout:step:shipping', function(data) {
        var saveAddress;
        if (data.use_address) {
          saveAddress = addresses.get(data.use_address);
        } else {
          saveAddress = address;
        }

        if (user.get('auth')) {
          data.email = user.get('email');
        }
        if (user.get('mailchimp')) {
          data.mailchimp_newsletter = true;
        }
        data.order = order.get('_id');
        data.type = 'Shipping';
        csrf().then(function() {
          return saveAddress.saveOrder(data);
        }).then(function(resp) {
          if (saveAddress.isValid()) {
            globalChannel.trigger('checkout:show', 'payment');
            return true;
          } else {
            stepView.triggerMethod('shipping:error');
            return false;
          }
        }).catch(function(err) {
          stepView.triggerMethod('shipping:error');
        });
      });

      self.listenTo(stepView, 'payment:create', function(data, cc_number, cc_exp, cc_cvc) {
        var saveAddress;
        if (data && data.use_address) {
          saveAddress = addresses.get(data.use_address);
        } else if (data) {
          saveAddress = address;
        } else {
          var shipping_address = order.get('shipping_address');
          saveAddress = address;
          data = {
            address1: shipping_address.address1,
            address2: shipping_address.address2,
            city: shipping_address.city,
            country: shipping_address.country,
            country_code: shipping_address.country_code,
            first_name: shipping_address.first_name,
            last_name: shipping_address.last_name,
            postcode: shipping_address.postcode,
            region: shipping_address.region,
            type: 'Billing',
            order: order.get('id')
          };
        }
        csrf().then(function() {
          return saveAddress.saveOrder(data);
        }).then(function(resp) {
          if (saveAddress.isValid()) {
            return stripeToken(saveAddress.toJSON(), cc_number, cc_exp, cc_cvc);
          } else {
            return false;
          }
        }).then(function(resp) {
          if (resp && resp.id) {
            var data = order.toJSON();
            data.token = resp.id;
            return createPayment(data);
          }
          return false;
        }).then(function(resp) {
          var payment = resp.body;
          if (payment.type === "StripeCardError") {
            stepView.triggerMethod('payment:error');
            if (payment.code == "incorrect_zip") {
              return message("error", "The postcode / zip code you supplied failed validation. Please make sure it matches the address the card is registered to.", 10000);
            } else {
              return message("error", payment.message, 10000);
            }
          }
          if (payment.type === "StripeInvalidRequest") {
            Rollbar.error(resp);
            return message("error", 'Opps a problem occured. If the problem persists please contact hello@baileyofsheffield.com');
          }
          if (payment && payment.details.paid) {
            globalChannel.trigger('checkout:success');
            GA.transaction(order.toJSON(), lineitems.toJSON());
          } else {
            message("error", 'Opps a problem occured. If the problem persists please contact hello@baileyofsheffield.com');
            stepView.triggerMethod('payment:error');
          }
        }).catch(function(err) {
          if (err && err.message) {
            message("error", err.message);
          } else {
            message("error", 'Opps a problem occured. If the problem persists please contact hello@baileyofsheffield.com');
          }
          stepView.triggerMethod('payment:error');
          Rollbar.error(err);
        });
      });

      self.listenTo(discountView, 'discount:apply', function(code) {
        discountCheck.isValid(code, order.get('id')).then(function(resp) {
          return order.fetch();
        }).then(function() {
          return discountView.triggerMethod('discount:applied');
        }).catch(function(err) {
          discountView.triggerMethod("discount:invalid", err.text);
          if (err.status != 400 && err.status != 404) {
            Rollbar.error(err);
          }
        });
      });
      self.listenTo(giftView, 'checkout:gift:wrapper', function(data) {
        csrf().then(function() {
          return order.updateCurrent(data);
        });
      });
      region.show(layoutView);

      // //Sockets
      // var newSailsSocket = io.sails.connect();
      //
      // newSailsSocket.on('connect', function() {
      //   newSailsSocket.get('/api/order/subscribe', {
      //     order: order.id
      //   });
      // });
      //
      // newSailsSocket.on('order', function(obj) {
      //   if (obj.verb == "updated" && obj.data.status == "Cart") {
      //     globalChannel.trigger('cart:show');
      //     message('error', window.t('Order.Checkout.Expired'), 10000);
      //   }
      // });
      GA.checkoutStep(stepNo, lineitems);
      meta(pageTitle, pageTitle);
      return true;
    } else {
      return globalChannel.trigger('checkout:login');
    }
  }).catch(function(err) {
    Rollbar.error(err);
  });
};
