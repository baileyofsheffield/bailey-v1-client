var PaymentModel = require('../../../entities/payment'),
  PaymentView = require('../views/payment/payment'),
  csrf = require('../../../config/csrf'),
  stripeToken = require('../../../config/stripeToken'),
  createPayment = require('../../../config/createPaymentRequest'),
  message = require('../../../config/message'),
  globalChannel = require('../../../config/globalChannel'),
	LoadingView = require('../../../common/loading');

module.exports = function(self, region, orderid, token) {
	region.show(new LoadingView());
	var payment = new PaymentModel();
	payment.fetch({
		data: {
			orderid: orderid,
			token: token
		}
	}).then(function() {
    var order = payment.get('order');
    var request = payment.get('request');

    var paymentView = new PaymentView({
      order: order,
      request: request
    });

    self.listenTo(paymentView, 'payment:create', function(cc_number, cc_exp, cc_cvc) {
      var billing;
      if (order.billing_address) {
        billing = order.billing_address;
      } else {
        billing = order.shipping_address;
      }
      stripeToken(billing, cc_number, cc_exp, cc_cvc).then(function (stripe) {
        var data = {
          stripe: stripe.id,
          order: order.id,
          request: request.id
        };
        return createPayment(data);
      }).then(function (resp) {
        if (resp.type === "StripeCardError") {
          paymentView.triggerMethod('payment:error');
          return message("error", resp.message, 10000);
        }
        if (resp.type === "StripeInvalidRequest") {
          Rollbar.error(resp);
          paymentView.triggerMethod('payment:error');
          return message("error", 'Opps a problem occured. If the problem persists please contact hello@baileyofsheffield.com');
        }
        if (resp && resp.details.paid) {
          globalChannel.trigger('payment:request:success');
        } else {
          paymentView.triggerMethod('payment:error');
          message("error", 'Opps a problem occured. If the problem persists please contact hello@baileyofsheffield.com');
        }
      }).catch(function(err) {
        if (err && err.message) {
          message("error", err.message);
        } else {
          message("error", 'Opps a problem occured. If the problem persists please contact hello@baileyofsheffield.com');
        }
        paymentView.triggerMethod('payment:error');
        Rollbar.error(err);
      });
    });

    region.show(paymentView);
	}).fail(function () {
    globalChannel.trigger('home:show');
	});
};
