var LoadingView = require('../../../common/loading'),
  SuccessView = require('../views/payment_success/view'),
  globalChannel = require('../../../config/globalChannel'),
  meta = require('../../../config/meta');

module.exports = function(self, region, slug) {
  region.show(new LoadingView());
  region.show(new SuccessView());
  meta('Payment success', 'Payment success');
};
