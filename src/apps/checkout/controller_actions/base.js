var app = require('../../../app'),
  globalChannel = require('../../../config/globalChannel');

module.exports = Mn.Object.extend({
  initialize: function() {
    return this.setHandlers();
  },
  setHandlers: function() {
    var _this = this;
    globalChannel.on('checkout:show', function(step) {
      app.navigate("checkout/" + step);
      return _this.show(step);
    });
    globalChannel.on('checkout:login', function() {
      app.navigate("checkout/login");
      return _this.login();
    });
    globalChannel.on('checkout:success', function() {
      app.navigate("checkout/success");
      return _this.success();
    });
    globalChannel.on('payment:request:success', function() {
      app.navigate("payment/success");
      return _this.payment_success();
    });
  }
});
