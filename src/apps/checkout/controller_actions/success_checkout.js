var LoadingView = require('../../../common/loading'),
	SuccessView = require('../views/success/view'),
	globalChannel = require('../../../config/globalChannel'),
  OrderModel = require('../../../entities/order'),
  csrf = require('../../../config/csrf'),
	meta = require('../../../config/meta');

module.exports = function(self, region, slug) {
	region.show(new LoadingView());

  var order = new OrderModel();
	var successView = new SuccessView();

	self.listenTo(successView, 'order:survey', function(data) {
    csrf().then(function () {
      return order.updateSurvey(data);
    }).then(function (resp) {
      successView.triggerMethod('close:survey');
    }).catch(function (err) {
      successView.triggerMethod('close:survey');
    });
	});

	region.show(successView);
	globalChannel.trigger('basket:reset');
	meta('Checkout success', 'Checkout success');
};
