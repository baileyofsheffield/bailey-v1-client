var UserModel = require('../../../entities/user'),
  LoadingView = require('../../../common/loading'),
  LayoutView = require('../views/login/layout'),
  LoginView = require('../../login/views/login/login'),
  CreateView = require('../../login/views/login/create'),
  ProgressView = require('../views/progress/view'),
  globalChannel = require('../../../config/globalChannel'),
  userChannel = require('../../../config/userChannel'),
  UserLogin = require('../../../entities/login'),
  userForgot = require('../../../config/userForgot'),
  csrf = require('../../../config/csrf'),
  message = require('../../../config/message'),
  meta = require('../../../config/meta');

module.exports = function(self, region) {
  region.show(new LoadingView());
  var createUser = new UserModel();
  var userLogin = new UserLogin();
  var loginView = new LoginView({
    model: userLogin
  });

  var createView = new CreateView({
    model: createUser
  });

  var layoutView = new LayoutView();

  var progressView = new ProgressView({
    step: 'login'
  });

  self.listenTo(layoutView, 'show', function() {
    layoutView.loginRegion.show(loginView);
    layoutView.createRegion.show(createView);
    layoutView.progressRegion.show(progressView);
  });

  self.listenTo(layoutView, 'guest:checkout', function() {
    userChannel.trigger('set:guest:user');
    globalChannel.trigger('checkout:show', 'shipping');
  });

  self.listenTo(loginView, 'form:submit', function(data) {
    csrf().then(function() {
      return userLogin.save(data);
    }).then(function(resp) {
      if (userLogin.isValid()) {
        userChannel.trigger('set:current:user', resp);
        if (resp.userid) {
          dataLayer.push({
            'userId': resp.userid
          });
        }
        return globalChannel.trigger('checkout:show', 'shipping');
      }
    }).catch(function(resp) {
      loginView.triggerMethod('button:reset');
      message('error', window.t(resp.responseText), 10000);
    });
  });

  self.listenTo(createView, 'form:submit', function(data) {
    csrf().then(function() {
      return createUser.signup(data);
    }).then(function(resp) {
      if (resp) {
        userChannel.trigger('set:current:user', resp);
        if (resp.userid) {
          dataLayer.push({
            'userId': resp.userid
          });
        }
        globalChannel.trigger('checkout:show', 'shipping');
        if (data.newsletter) {
          var popup_expire = 60*60*24*15;
          Cookies.set('viewedOuibounceModal', true, { expires: popup_expire });
        }
        fbq('track', 'CompleteRegistration');
      }
    }).catch(function(err) {
      createView.triggerMethod('button:reset');
      if (err) {
        message('error', err.responseText);
      }
    });
  });

  self.listenTo(loginView, 'forgot:show', function(data) {
    globalChannel.trigger('forgot:show');
  });

  region.show(layoutView);
  meta('Checkout login', 'Checkout login');
};
