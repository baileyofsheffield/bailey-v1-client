var BaseRouter = require('../../config/router');

module.exports = BaseRouter.extend({
  appRoutes: {
    "checkout/login": "login",
    "checkout/success": "success",
    "checkout/:step": "show",
    "payment/:orderid/:token": "payment",
    "payment/success": "payment_success"
  }
});
