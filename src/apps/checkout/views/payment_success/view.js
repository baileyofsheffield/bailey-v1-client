var tpl = require('./templates/view.hbs');

module.exports = Mn.ItemView.extend({
  template: tpl,
  tagName: 'section',
  className: 'page-layout main-wrapper'
});
