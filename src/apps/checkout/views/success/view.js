var tpl = require('./templates/view.hbs');

module.exports = Mn.ItemView.extend({
	template: tpl,
	tagName: 'section',
	className: 'page-layout main-wrapper',
	events: {
		'click .submit-survey': 'submitSurvey',
    'click .close-model': 'onCloseSurvey'
	},
	submitSurvey: function(e) {
		e.preventDefault();
		var data = Backbone.Syphon.serialize(this);
    this.trigger('order:survey', data);
	},
  onCloseSurvey: function () {
    this.$('#order-survey').hide();
  }
});
