var Cookies = require('cookies-js'),
  tpl = require('./templates/payment.hbs'),
  message = require('../../../../config/message'),
  payment = require('jquery.payment'),
  FormBehavior = require('../../../../behaviors/formValidate'),
  globalChannel = require('../../../../config/globalChannel'),
  self;
module.exports = Mn.ItemView.extend({
  template: tpl,
  tagName: 'div',
  id: 'payment-form',
  className: 'checkout-pane container',
  events: {
    'change #addressSelect': 'addressAuto',
    'change #billingCountryCode': 'changeBillingCountry',
    'click #different_shipping': 'showBilling',
    'click button.js-next': 'submitPayment',
    'keyup .card-pre-check': 'cardPreCheck'
  },
  behaviors: {
    formValidate: {
      behaviorClass: FormBehavior
    }
  },
  serializeData: function() {
    return {
      order: this.options.order.toJSON(),
      countries: this.options.countries.toJSON(),
      addresses: this.options.addresses.toJSON()
    };
  },
  onShow: function() {
    this.$('.cc-number').payment('formatCardNumber');
    this.$('.cc-exp').payment('formatCardExpiry');
    this.$('.cc-cvc').payment('formatCardCVC');
    var order = this.options.order.toJSON();
    if (order.billing_address && order.billing_address.country_code && order.billing_address.country) {
      $("#billingCountryCode > option[value=" + order.billing_address.country_code + "]").prop("selected", true);
      $("#billingCountry").val(order.billing_address.country);
      if (order.billing_address.country == 'Ireland') {
        $('#postcode').removeAttr('required');
        $('#postcode').parent().removeClass('has-error');
      }
    } else if (Cookies.enabled) {
      var country = Cookies.get('country_code');
      $("#billingCountryCode > option[value=" + country + "]").prop("selected", true);
      var selectCountry = this.options.countries.where({
        cca2: country
      });
      if (selectCountry.length > 0) {
        var countryName = selectCountry[0].get('name');
        $("#billingCountry").val(countryName);
      }
    }
  },
  addressAuto: function(e) {
    e.preventDefault();
    var addressId = this.$(e.target).val();
    if (!addressId) return;
    var getAddress = this.options.addresses.where({
      _id: addressId
    });
    var addressValues = getAddress[0].toJSON();
    _.each(addressValues, function(value, key) {
      if (key === 'country_code') {
        $("#billingCountryCode > option[value=" + value + "]").prop("selected", true);
      } else {
        $("input[name*='" + key + "']").val(value).change();
      }
    });
    $("#billingCountry").val(addressValues.country);
    this.model.set({
      country_code: addressValues.country_code,
      country: addressValues.country
    }, {
      forceUpdate: true
    });
    if (addressValues.country == 'Ireland') {
      $('#postcode').removeAttr('required');
      $('#postcode').parent().removeClass('has-error');
      $('#postcode').next('.input-error').remove();
    } else {
      $('#postcode').attr('required');
      this.triggerMethod('validate:field', $('#postcode'));
    }
  },
  changeBillingCountry: function(e) {
    e.preventDefault();
    var countryCode = this.$(e.target).val();
    var getContry = this.options.countries.where({
      cca2: countryCode
    });
    var countryName = getContry[0].get('name');
    $("#billingCountry").val(countryName);
    this.model.set({
      country_code: countryCode,
      country: countryName
    }, {
      forceUpdate: true
    });
    if (countryName == 'Ireland') {
      $('#postcode').removeAttr('required');
      $('#postcode').parent().removeClass('has-error');
      $('#postcode').next('.input-error').remove();
    } else {
      $('#postcode').attr('required');
      this.triggerMethod('validate:field', $('#postcode'));
    }

  },
  showBilling: function(e) {
    var that = this;
    var checked = this.$('#different_shipping').is(':checked');
    var country;
    if (checked) {
      this.$('.billing-fields').slideToggle('fast');
      this.$('.dropdown').removeClass('disabled');
      country = this.$('#billingCountryCode').val();
      $(".billing-fields").find('.form-control').each(function() {
        if ($(this).hasClass('pre-check')) {
          that.triggerMethod("validate:error", this);
        }
        if ($(this).prop('disabled')) {
          $(this).prop('disabled', false);
        }
      });
    } else {
      this.$('.billing-fields').slideToggle('fast');
      this.$('.dropdown').addClass('disabled');
      country = this.$('#billingCountryCode').val();
      $(".billing-fields").find('.form-control').each(function(index, value) {
        if (!$(this).prop('disabled')) {
          $(this).prop('disabled', true);
        }
      });
    }
  },
  cardPreCheck: function(e) {
    e.preventDefault();
    var cardType = $.payment.cardType($('.cc-number').val());
    this.cardInputError($('.cc-number'), !$.payment.validateCardNumber($('.cc-number').val()));
    this.cardInputError($('.cc-exp'), !$.payment.validateCardExpiry($('.cc-exp').payment('cardExpiryVal')));
    this.cardInputError($('.cc-cvc'), !$.payment.validateCardCVC($('.cc-cvc').val(), cardType));
  },
  cardInputError: function(field, erred) {
    var group = field.parent();
    group.toggleClass('has-error', erred);
    group.toggleClass('has-success', !erred);
    if (group.find('.input-error').length === 0 && erred) {
      group.find('.input-success').remove();
      group.find('.form-control:visible').after('<span class="input-error"><svg><use xlink:href="/svg/sprite.svg#info"></use></svg></span>');
    } else if (group.find('.input-success').length === 0 && !erred) {
      group.find('.input-error').remove();
      group.find('.form-control:visible').after('<span class="input-success"><svg><use xlink:href="/svg/sprite.svg#tick"></use></svg></span>');
    }
    return field;
  },
  submitPayment: function(e) {
    e.preventDefault();
    var cardType = $.payment.cardType($('.cc-number').val());
    this.cardInputError($('.cc-number'), !$.payment.validateCardNumber($('.cc-number').val()));
    this.cardInputError($('.cc-exp'), !$.payment.validateCardExpiry($('.cc-exp').payment('cardExpiryVal')));
    this.cardInputError($('.cc-cvc'), !$.payment.validateCardCVC($('.cc-cvc').val(), cardType));
    if ($(".checkout-layout .has-error").length > 0) return;
    this.$('.js-next').prop('disabled', true).text('Please wait...');
    var checked = this.$('#different_shipping').is(':checked');
    var billing;
    if (checked) {
      billing = Backbone.Syphon.serialize(this.$('#billing')[0]);
      billing.type = 'Billing';
      billing.order = this.options.order.get('id');
    }
    this.trigger('payment:create', billing, $('.cc-number').val(), $('.cc-exp').val(), $('.cc-cvc').val());
  },
  onPaymentError: function() {
    this.$('.js-next').prop('disabled', false).text('Make Payment');
  }
});
