var tpl = require('./templates/discount.hbs'),
  globalChannel = require('../../../../config/globalChannel');
module.exports = Mn.ItemView.extend({
  template: tpl,
  tagName: 'div',
  id: 'discount',
  className: 'checkout-space',
  events: {
    "click .apply-discount": "applyDiscount"
  },
  modelEvents: {
    'sync': 'render'
  },
  serializeData: function() {
    return {
      order: this.options.model.toJSON()
    };
  },
  applyDiscount: function(e) {
    e.preventDefault();
    var code = this.$('#discountCode').val();
    if (!code) {
      return this.onDiscountInvalid();
    }
    var control = this.$('#discountCode');
    var group = control.parents('.form-group');
    group.removeClass('has-error');
    group.find('.input-error').remove();
    this.$('.apply-discount').prop('disabled', true).text('Checking code...');
    this.trigger("discount:apply", code);
  },
  onDiscountInvalid: function(error) {
    this.$('.apply-discount').prop('disabled', false).text('Apply');
    var control = this.$('#discountCode');
    var group = control.parents('.form-group');
    group.addClass('has-error');
    if (group.find('.input-error').length === 0) {
      group.find('.form-control:visible').after('<span class="input-error"><svg><use xlink:href="/svg/sprite.svg#info"></use></svg></span>');
    }
  },
  onDiscountApplied: function () {
    this.$('.apply-discount').prop('disabled', false).text('Apply');
  }
});
