var tpl = require('./templates/carts.hbs'),
  Cart = require('./cart');

module.exports = Mn.CompositeView.extend({
  className: 'checkout-summary',
  template: tpl,
  childView: Cart,
  childViewContainer: "ul.cart-list",
  modelEvents: {
    'change': 'render'
  },
  serializeData: function() {
    return {
      order: this.options.model.toJSON()
    };
  }
});
