var tpl = require('./templates/layout.hbs');

module.exports = Mn.LayoutView.extend({
  className: "checkout-layout main-wrapper",
  template: tpl,
  regions: {
    progressRegion: ".checkout-progress",
    cartsRegion: ".cart-items",
    stepRegion: ".checkout-step"
  },
  onRender: function() {
    $('body').addClass('checkout-page');
  }
});
