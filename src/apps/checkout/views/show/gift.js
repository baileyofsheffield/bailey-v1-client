var tpl = require('./templates/gift.hbs'),
	globalChannel = require('../../../../config/globalChannel');
module.exports = Mn.ItemView.extend({
	template: tpl,
	tagName: 'div',
	id: 'gift',
	maxChar: 130,
	maxCharWarn: 20,
	className: 'checkout-space',
	events: {
		"keydown .gift-msg": "updateCount",
		"click #gift_wrap": "showOptions",
		"click .add-gift": "addMessage",
		"click .edit-gift": "editMessage"
	},
	modelEvents: {
		'change': 'render'
	},
	serializeData: function() {
		return {
			order: this.options.model.toJSON(),
			settings: this.options.settings.toJSON()
		};
	},
	onShow: function() {
		this.setMessage();
	},
	onRender: function () {
		this.setLayout();
	},
	setMessage: function () {
		var msg = document.getElementsByClassName("gift-msg")[0],
			charLeftLabel = "char-left",
			charLeft = document.getElementsByClassName(charLeftLabel)[0];
		charLeft.innerHTML = this.maxChar;
		this.msg = msg;
		this.charLeft = charLeft;
		this.charLeftLabel = charLeftLabel;
	},
	setLayout: function () {
		var gift_wrap = this.model.get('gift_wrap');
		var gift_wrap_message = this.model.get('gift_wrap_message');
		this.$('.add-gift').prop('disabled', false).text('Add message');
		if (gift_wrap) {
			this.$('.gift-options').show();
		} else {
			this.$('.gift-options').hide();
		}
		if (gift_wrap_message) {
			this.$('.gift-view').show();
			this.$('.gift-edit').hide();
		} else {
			this.$('.gift-view').hide();
			this.$('.gift-edit').show();
		}
	},
	showOptions: function(e) {
		this.$('.gift-options').toggle();
		this.setMessage();
		if (!$(e.target).is(':checked')) {
			var data = Backbone.Syphon.serialize(this.$('#wrapping')[0]);
			data.gift_wrap_message = "";
			this.$('.add-gift').prop('disabled', true).text('Please wait...');
			this.trigger('checkout:gift:wrapper', data);
		}
	},
	updateCount: function(e) {
		var that = this;
		setTimeout(function() {
			that.charLeft.innerHTML = that.maxChar - that.msg.value.length;
			var warnLabel = that.msg.value.length >= that.maxChar - that.maxCharWarn ? " warning" : "";
			that.charLeft.className = that.charLeftLabel + warnLabel;
			if (that.msg.value.length > that.maxChar) {
				that.$('.add-gift').prop('disabled', true);
			} else {
				that.$('.add-gift').prop('disabled', false);
			}
		}, 1);
	},
	addMessage: function(e) {
		e.preventDefault();
		var data = Backbone.Syphon.serialize(this.$('#wrapping')[0]);
		if (data && data.gift_wrap_message) {
			data.gift_wrap_message = "<p>" + data.gift_wrap_message + "</p>";
			data.gift_wrap_message = data.gift_wrap_message.replace(/\n\n/g, "</p><p>");
			data.gift_wrap_message = data.gift_wrap_message.replace(/\n/g, "<br>");
		}
		this.$('.add-gift').prop('disabled', true).text('Please wait...');
		this.trigger('checkout:gift:wrapper', data);
	},
	editMessage: function(e) {
		e.preventDefault();
		this.$('.gift-view').toggle();
		this.$('.gift-edit').toggle();
		this.setMessage();
		this.updateCount();
	}
});
