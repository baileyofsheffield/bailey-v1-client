var Cookies = require('cookies-js'),
  tpl = require('./templates/shipping.hbs'),
  FormBehavior = require('../../../../behaviors/formValidate'),
  globalChannel = require('../../../../config/globalChannel');

module.exports = Mn.LayoutView.extend({
  template: tpl,
  events: {
    'change #addressSelect': 'addressAuto',
    'change #shippingCountryCode': 'changeShippingCountry',
    'click .js-next': 'nextStep'
  },
  regions: {
    discountRegion: ".checkout-discount",
    giftRegion: ".checkout-gift"
  },
  behaviors: {
    formValidate: {
      behaviorClass: FormBehavior
    }
  },
  serializeData: function() {
    return {
      order: this.options.order.toJSON(),
      countries: this.options.countries.toJSON(),
      addresses: this.options.addresses.toJSON(),
      auth: this.options.auth
    };
  },
  onShow: function() {
    var order = this.options.order.toJSON();
    if (order.shipping_address && order.shipping_address.country_code && order.shipping_address.country) {
      $("#shippingCountryCode > option[value=" + order.shipping_address.country_code + "]").prop("selected", true);
      $("#shippingCountry").val(order.shipping_address.country);
      if (order.shipping_address.country == 'Ireland') {
        $('#postcode').removeAttr('required');
      }
    } else if (Cookies.enabled) {
      var country = Cookies.get('country_code');
      $("#shippingCountryCode > option[value=" + country + "]").prop("selected", true);
      var selectCountry = this.options.countries.where({
        cca2: country
      });
      if (selectCountry.length > 0) {
        var countryName = selectCountry[0].get('name');
        $("#shippingCountry").val(countryName);
      }
    }
  },
  addressAuto: function(e) {
    e.preventDefault();
    var addressId = this.$(e.target).val();
    if (!addressId) return;
    var getAddress = this.options.addresses.where({
      _id: addressId
    });
    var addressValues = getAddress[0].toJSON();
    _.each(addressValues, function(value, key) {
      if (key === 'country_code') {
        $("#shippingCountryCode > option[value=" + value + "]").prop("selected", true);
      } else {
        $("input[name*='" + key + "']").val(value).change();
      }
    });
    $("#shippingCountry").val(addressValues.country);
    this.model.set({
      country_code: addressValues.country_code,
      country: addressValues.country
    }, {
      forceUpdate: true
    });
    if (addressValues.country == 'Ireland') {
      $('#postcode').removeAttr('required');
      $('#postcode').parent().removeClass('has-error');
      $('#postcode').next('.input-error').remove();
    } else {
      $('#postcode').attr('required');
      this.triggerMethod('validate:field', $('#postcode'));
    }
  },
  changeShippingCountry: function(e) {
    e.preventDefault();
    var countryCode = this.$(e.target).val();
    var getContry = this.options.countries.where({
      cca2: countryCode
    });
    var countryName = getContry[0].get('name');
    $("#shippingCountry").val(countryName);
    this.model.set({
      country_code: countryCode,
      country: countryName
    }, {
      forceUpdate: true
    });
    if (countryName == 'Ireland') {
      $('#postcode').removeAttr('required');
      $('#postcode').parent().removeClass('has-error');
      $('#postcode').next('.input-error').remove();
    } else {
      $('#postcode').attr('required');
      this.triggerMethod('validate:field', $('#postcode'));
    }

    var obj = this.options.order.toJSON();
    if (countryCode === "GB") {
      if (this.options.has_christmas) {
        obj.shipping_price = 500;
      } else {
        obj.shipping_price = 0;
      }
    } else {
      obj.shipping_price = 1250;
    }

    if (obj.discount_percent && obj.discount_percent > 0) {
      var discount = Math.ceil((obj.subtotal / 100) * obj.discount_percent);
      obj.discount_amount = discount;
      obj.total = obj.subtotal + obj.shipping_price - discount;
    } else if (obj.discount_amount) {
      obj.total = obj.subtotal + obj.shipping_price - obj.discount_amount;
      delete obj.discount_percent;
    } else {
      obj.total = obj.subtotal + obj.shipping_price;
    }
    this.options.order.set({
      shipping_price: obj.shipping_price,
      total: obj.total
    });
  },
  changeShipping: function(e) {
    var $form = this.$el;
    if ($(e.target).is(':checked')) {
      $form.find('.shipping-address :input:disabled').removeAttr('disabled');
    } else {
      $form.find('.shipping-address input[type=text]').attr('disabled', 'disabled').val('');
    }
  },
  nextStep: function(e) {
    e.preventDefault();
    var data = Backbone.Syphon.serialize(this.$('#shipping_address_form')[0]);
    this.$('.js-next').prop('disabled', true).text('Please wait...');
    this.trigger('checkout:step:shipping', data);
  },
  onShippingError: function() {
    this.$('.js-next').prop('disabled', false).text('Make Payment');
  }
});
