var tpl = require('./templates/cart.hbs');

module.exports = Mn.ItemView.extend({
  tagName: "li",
  template: tpl,
  className: 'item',
});
