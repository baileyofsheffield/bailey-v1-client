var tpl = require('./templates/view.hbs');

module.exports = Mn.ItemView.extend({
  template: tpl,
  tagName: 'ul',
  className: 'progress-indicator',
  onShow: function() {
    var step = this.options.step;
    this.$('.step-' + step).addClass('active');
  }
});
