var payment = require('jquery.payment'),
  tpl = require('./templates/payment.hbs');

module.exports = Mn.ItemView.extend({
  className: "checkout-layout payment-request main-wrapper",
  template: tpl,
  events: {
    'click button.js-next': 'submitPayment',
    'keyup .card-pre-check': 'cardPreCheck'
  },
  onRender: function() {
    $('body').addClass('checkout-page');
  },
  serializeData: function() {
    return {
      order: this.options.order,
      request: this.options.request
    };
  },
  onShow: function() {
    this.$('.cc-number').payment('formatCardNumber');
    this.$('.cc-exp').payment('formatCardExpiry');
    this.$('.cc-cvc').payment('formatCardCVC');

  },
  cardPreCheck: function (e) {
    e.preventDefault();
    var cardType = $.payment.cardType($('.cc-number').val());
    this.cardInputError($('.cc-number'), !$.payment.validateCardNumber($('.cc-number').val()));
    this.cardInputError($('.cc-exp'), !$.payment.validateCardExpiry($('.cc-exp').payment('cardExpiryVal')));
    this.cardInputError($('.cc-cvc'), !$.payment.validateCardCVC($('.cc-cvc').val(), cardType));
  },
  cardInputError: function(field, erred) {
    var group = field.parent();
    group.toggleClass('has-error', erred);
    group.toggleClass('has-success', !erred);
    if (group.find('.input-error').length === 0 && erred) {
      group.find('.input-success').remove();
      group.find('.form-control:visible').after('<span class="input-error"><svg><use xlink:href="/svg/sprite.svg#info"></use></svg></span>');
    } else if (group.find('.input-success').length === 0 && !erred) {
      group.find('.input-error').remove();
      group.find('.form-control:visible').after('<span class="input-success"><svg><use xlink:href="/svg/sprite.svg#tick"></use></svg></span>');
    }
    return field;
  },
  submitPayment: function(e) {
    e.preventDefault();
    var cardType = $.payment.cardType($('.cc-number').val());
    this.cardInputError($('.cc-number'), !$.payment.validateCardNumber($('.cc-number').val()));
    this.cardInputError($('.cc-exp'), !$.payment.validateCardExpiry($('.cc-exp').payment('cardExpiryVal')));
    this.cardInputError($('.cc-cvc'), !$.payment.validateCardCVC($('.cc-cvc').val(), cardType));
    if ($(".checkout-layout .has-error").length > 0) return;
    this.$('.js-next').prop('disabled', true).text('Please wait...');
    this.trigger('payment:create', $('.cc-number').val(), $('.cc-exp').val(), $('.cc-cvc').val());
  },
  onPaymentError: function() {
    this.$('.js-next').prop('disabled', false).text('Make Payment');
  }
});
