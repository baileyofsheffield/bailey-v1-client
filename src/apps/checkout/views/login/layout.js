var tpl = require('./templates/layout.hbs');

module.exports = Mn.LayoutView.extend({
  template: tpl,
  className: 'account-login checkout-pane main-wrapper',
  regions: {
    progressRegion: ".checkout-progress",
    loginRegion: ".user-login",
    createRegion: ".create-account"
  },
  triggers: {
    'click button.js-guest': 'guest:checkout'
  },
  onShow: function() {
    $('body').addClass('bg-grey');
  },
});
