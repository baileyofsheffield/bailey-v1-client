var app = require('../../app'),
  CheckoutControllerBase = require('./controller_actions/base'),
  ShowCheckout = require('./controller_actions/show_checkout'),
  LoginCheckout = require('./controller_actions/login_checkout'),
  SuccessCheckout  = require('./controller_actions/success_checkout'),
  SuccessPayment  = require('./controller_actions/success_payment'),
  ShowPayment = require('./controller_actions/show_payment');

module.exports = CheckoutControllerBase.extend({
  show: function(step) {
    app.startSubApp("CheckoutApp");
    ShowCheckout(this, app.main, step);
  },
  login: function() {
    app.startSubApp("CheckoutApp");
    LoginCheckout(this, app.main);
  },
  success: function() {
    app.startSubApp("CheckoutApp");
    SuccessCheckout(this, app.main);
  },
  payment: function(orderid, token) {
    app.startSubApp("CheckoutApp");
    ShowPayment(this, app.main, orderid, token);
  },
  payment_success: function() {
    app.startSubApp("CheckoutApp");
    SuccessPayment(this, app.main);
  },
});
