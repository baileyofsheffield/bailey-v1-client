var app = require('../../app'),
  CheckoutRouter = require('./router'),
  CheckoutController = require('./controller'),
  CheckoutModule;

CheckoutModule = Mn.Module.extend({
  startWithParent: false,
  initialize: function() {
    this.controller = new CheckoutController();
    this.router = new CheckoutRouter({
      controller: this.controller
    });
  },
  onStop: function() {
    this.controller.destroy();
  }
});

module.exports = CheckoutModule;
