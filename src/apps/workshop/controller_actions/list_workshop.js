var app = require('../../../app'),
  WorkshopCollection = require('../../../entities/workshops'),
  LoadingView = require('../../../common/loading'),
  LayoutView = require('../views/list/layout'),
  PostsView = require('../views/list/posts'),
  MenusView = require('../views/list/menus'),
  LinksView = require('../views/list/links'),
  meta = require('../../../config/meta'),
  globalChannel = require('../../../config/globalChannel');

module.exports = function(self, region, options) {
  var loadingView = new LoadingView();
  region.show(loadingView);

  var posts = new WorkshopCollection();

  var layoutView = new LayoutView();

  posts.fetch().then(function() {

    var menusView = new MenusView();

    var postsView = new PostsView({
      collection: posts
    });

    var linksView = new LinksView();

    self.listenTo(layoutView, 'show', function(){
      layoutView.menuRegion.show(menusView);
      layoutView.postRegion.show(postsView);
      layoutView.linkRegion.show(linksView);
    });

    region.show(layoutView);
    meta('Workshop', 'Workshop', 'Bailey of Sheffield - Fusing traditional jewellery techniques, the latest precision engineering, and a thoughtful and sincere approach to innovative design.');
  });
};
