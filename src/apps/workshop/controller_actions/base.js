var app = require('../../../app'),
  globalChannel = require('../../../config/globalChannel');

module.exports = Mn.Object.extend({
  initialize: function() {
    return this.setHandlers();
  },
  setHandlers: function() {
    var _this = this;
    globalChannel.on('workshop:list', function() {
      app.navigate("workshop");
      return _this.list();
    });
    globalChannel.on('workshop:show', function(slug) {
      app.navigate('workshop/' + slug);
      return _this.show(slug);
    });
  }
});
