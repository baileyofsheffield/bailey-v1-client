var tpl = require('./templates/layout.hbs');

module.exports = Mn.LayoutView.extend({
  className: "workshop-layout main-wrapper",
  template: tpl,
  regions: {
    menuRegion: ".workshop-nav",
    postRegion: ".posts",
    linkRegion: ".workshop-links"
  },
  onRender: function() {
    $('body').addClass('workshop');
  }
});
