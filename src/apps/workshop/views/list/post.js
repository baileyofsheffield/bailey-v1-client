var tpl = require('./templates/post.hbs');

module.exports = Mn.ItemView.extend({
  template: tpl,
  className: 'post-item',
  events: {
    "click a.link": "linkClicked",
  },

  linkClicked: function(e){
    e.preventDefault();
    this.trigger("post:show", this.model);
  }
});
