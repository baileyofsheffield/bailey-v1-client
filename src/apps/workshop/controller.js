var app = require('../../app'),
  WorkshopControllerBase = require('./controller_actions/base'),
  ShowWorkshop = require('./controller_actions/show_workshop'),
  ListWorkshop = require('./controller_actions/list_workshop'),
  globalChannel = require('../../config/globalChannel');

module.exports = WorkshopControllerBase.extend({
  show: function(slug) {
    app.startSubApp("WorkshopApp");
    ShowWorkshop(this, app.main, slug);
  },
  list: function(lang) {
    app.startSubApp("WorkshopApp");
    ListWorkshop(this, app.main);
    globalChannel.trigger("set:active:header", "workshop");
  }
});
