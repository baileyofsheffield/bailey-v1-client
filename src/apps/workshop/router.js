var BaseRouter = require('../../config/router');

module.exports = BaseRouter.extend({
  appRoutes: {
    "workshop": "list",
    "workshop/:slug": "show"
  }
});
