var app = require('../../app'),
  WorkshopRouter = require('./router'),
  WorkshopController = require('./controller'),
  WorkshopModule;

WorkshopModule = Mn.Module.extend({
  startWithParent: false,
  initialize: function() {
    this.controller = new WorkshopController();
    this.router = new WorkshopRouter({
      controller: this.controller
    });
  },
  onStop: function() {
    this.controller.destroy();
  }
});

module.exports = WorkshopModule;
