var app = require('../../../app'),
	OrderModel = require('../../../entities/order'),
	LineItemCollection = require('../../../entities/lineitems'),
	LoadingView = require('../../../common/loading'),
	LayoutView = require('../views/show/layout'),
	CartsView = require('../views/show/carts'),
	NoCartView = require('../views/show/no_cart'),
	globalChannel = require('../../../config/globalChannel'),
	csrf = require('../../../config/csrf'),
	GA = require('../../../config/GA'),
	message = require('../../../config/message'),
	meta = require('../../../config/meta');

module.exports = function(self, region) {
	region.show(new LoadingView());
	var lineitems = new LineItemCollection();
	var order = new OrderModel();
	var layoutView = new LayoutView();
	Promise.all([order.fetch(), lineitems.fetch()]).then(function() {
		var cartsView = new CartsView({
			collection: lineitems,
			model: order
		});

		self.listenTo(layoutView, 'show', function() {
			layoutView.cartsRegion.show(cartsView);
		});

		self.listenTo(cartsView, 'childview:cart:update', function(childview, model, qty, quick) {
			var line_item = model.toJSON();
			csrf().then(function() {
				return model.save({
					qty: qty,
					engraving_options: line_item.engraving_options,
					custom: line_item.custom,
					product: line_item.product ? line_item.product._id : '',
					custom_items: line_item.custom_items,
					product_display: line_item.product_display._id ? line_item.product_display._id : line_item.product_display
				}, {
					patch: true
				});
			}).then(function(resp) {
				globalChannel.trigger('basket:update', resp.item, resp.order);
				model.set(resp.item);
				return order.set(resp.order);
			}).catch(function (err) {
				if (err && err.responseJSON.error) {
					message("error", window.t(err.responseJSON.error.message), 10000);
					return globalChannel.trigger('cart:show');
				}
			});
		});

		self.listenTo(cartsView, 'childview:cart:remove', function(childview, model) {
			csrf().then(function() {
				globalChannel.trigger('cart:removed', model);
				GA.removeCart(model.toJSON());
				return model.destroy();
			}).then(function(resp) {
				return order.set(resp.order);
			}).then(function(resp) {
				globalChannel.trigger('basket:remove', model, order.toJSON());
				if (lineitems.length === 0) {
					return region.show(new NoCartView());
				}
				return true;
			});
		});

		self.listenTo(cartsView, 'shop:list', function() {
			globalChannel.trigger('shop:list');
		});

		self.listenTo(cartsView, 'checkout:show', function() {
			csrf().then(function(resp) {
				return order.save({
					status: 'Checkout'
				});
			}).then(function(resp) {
				return globalChannel.trigger('checkout:show', 'shipping');
			}).catch(function (err) {
				if (err) {
					message("error", err.responseText, 10000);
					return globalChannel.trigger('cart:show');
				}
			});
		});
		meta('Bag', 'Bag');
		if (lineitems.length === 0) {
			return region.show(new NoCartView());
		} else {
			GA.checkoutStep(1, lineitems);
			return region.show(layoutView);
		}
	}).catch(function(err) {
		return console.log(err);
	});
};
