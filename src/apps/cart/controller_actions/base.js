var app = require('../../../app'),
  globalChannel = require('../../../config/globalChannel');

module.exports = Mn.Object.extend({
  initialize: function() {
    return this.setHandlers();
  },
  setHandlers: function() {
    var _this = this;
    globalChannel.on('cart:show', function() {
      app.navigate("bag");
      return _this.show();
    });
  }
});
