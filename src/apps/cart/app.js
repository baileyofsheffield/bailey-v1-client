var app = require('../../app'),
  CartRouter = require('./router'),
  CartController = require('./controller'),
  CartModule;

CartModule = Mn.Module.extend({
  startWithParent: false,
  initialize: function() {
    this.controller = new CartController();
    this.router = new CartRouter({
      controller: this.controller
    });
  },
  onStop: function() {
    this.controller.destroy();
  }
});

module.exports = CartModule;
