var app = require('../../app'),
  CartControllerBase = require('./controller_actions/base'),
  ShowCart = require('./controller_actions/show_cart');

module.exports = CartControllerBase.extend({
  show: function() {
    app.startSubApp("CartApp");
    ShowCart(this, app.main);
  }
});
