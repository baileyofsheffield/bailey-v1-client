var tpl = require('./templates/no_cart.hbs');

module.exports = Mn.ItemView.extend({
  className: "cart main-wrapper",
  template: tpl
});
