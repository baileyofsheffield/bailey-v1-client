var tpl = require('./templates/layout.hbs');

module.exports = Mn.LayoutView.extend({
  className: "cart-layout main-wrapper",
  template: tpl,
  regions: {
    cartsRegion: ".cart-items",
    actionsRegion: ".cart-actions"
  },
  onRender: function () {
    $('body').addClass('cart-page');
  }
});
