var tpl = require('./templates/cart.hbs');

var waitForPause, pauseDelay = 1000, self;
module.exports = Mn.ItemView.extend({
  tagName: "li",
  template: tpl,
  className: 'item',
  events: {
    'click button.remove-cart': 'cartRemove',
    'click .cart-add': 'cartAdd',
    'click .cart-minus': 'cartMinus'
  },
  initialize: function () {
    self = this;
  },
  cartRemove: function (e) {
    e.preventDefault();
    this.trigger('cart:remove', this.model);
  },
  cartAdd: function (e) {
    e.preventDefault();
    var product = this.model.get('product');
    var that = this;
    var qty = this.$('#qty').val();
    if (product && qty == product.stock) return;
    if (qty == 10) return;
    var newQty = ++qty;
    this.$('#qty').val(newQty);
    this.$('.cart-value').html('Updating...');
    clearTimeout(waitForPause);
    this.disableButton();
    waitForPause = setTimeout(function() {
      self.trigger('cart:update', that.model, newQty, false);
    }, pauseDelay);
  },
  cartMinus: function (e) {
    e.preventDefault();
    var that = this;
    var qty = this.$('#qty').val();
    if (qty <= 1) return;
    var newQty = --qty;
    this.$('#qty').val(newQty);
    this.$('.cart-value').html('Updating...');
    clearTimeout(waitForPause);
    this.disableButton();
    waitForPause = setTimeout(function() {
      self.trigger('cart:update', that.model, newQty, false);
    }, pauseDelay);
  },
  disableButton: function () {
    $('.show-checkout').prop('disabled', true).text("Cart updating...");
  }
});
