var Cart = require('./cart'),
  tpl = require('./templates/carts.hbs');

module.exports = Mn.CompositeView.extend({
  className: 'container',
  template: tpl,
  childView: Cart,
  childViewContainer: "ul.cart-list",
  modelEvents: {
    'sync change': 'render'
  },
  triggers: {
    'click button.show-checkout': 'checkout:show',
    'click button.shop-continue': 'shop:list'
  }
});
