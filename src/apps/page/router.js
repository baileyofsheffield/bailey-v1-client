var BaseRouter = require('../../config/router');

module.exports = BaseRouter.extend({
  appRoutes: {
    "look-book": "listLookBook",
    "story": "story",
    "portland-works": "portland",
    "meet-the-team": "team",
    "our-sheffield": "sheffield",
    "workshop": "workshop",
    "harry-stainless-steel": "harry",
    "testimonials": "review",
    "policy/:slug": "page",
    "technical/:slug": "page",
    "essentials/:slug": "page"
  }
});
