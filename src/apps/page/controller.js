var app = require('../../app'),
  PageControllerBase = require('./controller_actions/base'),
  ListLookBook = require('./controller_actions/list_lookbook'),
  ShowPage = require('./controller_actions/show_page'),
  ShowStory = require('./controller_actions/show_story'),
  OtherPage = require('./controller_actions/show_other'),
  ShowPortland = require('./controller_actions/show_portland'),
  ShowWorkshop = require('./controller_actions/show_workshop'),
  ListReview = require('./controller_actions/list_review'),
  globalChannel = require('../../config/globalChannel');

module.exports = PageControllerBase.extend({
  listLookBook: function () {
    app.startSubApp("PageApp");
    ListLookBook(this, app.main);
    globalChannel.trigger("set:active:header", "look-book");
  },
  page: function(slug) {
    app.startSubApp("PageApp");
    ShowPage(this, app.main, slug);
  },
  team: function() {
    app.startSubApp("PageApp");
    OtherPage(this, app.main, 'meet-the-team');
  },
  sheffield: function() {
    app.startSubApp("PageApp");
    OtherPage(this, app.main, 'our-sheffield');
  },
  harry: function() {
    app.startSubApp("PageApp");
    OtherPage(this, app.main, 'harry-stainless-steel');
  },
  story: function() {
    app.startSubApp("PageApp");
    ShowStory(this, app.main);
    globalChannel.trigger("set:active:header", "story");
  },
  portland: function() {
    app.startSubApp("PageApp");
    ShowPortland(this, app.main);
  },
  workshop: function() {
    app.startSubApp("PageApp");
    ShowWorkshop(this, app.main);
    globalChannel.trigger("set:active:header", "workshop");
  },
  review: function() {
    app.startSubApp("PageApp");
    ListReview(this, app.main);
  }
});
