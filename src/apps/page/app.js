var app = require('../../app'),
  PageRouter = require('./router'),
  PageController = require('./controller'),
  PageModule;

PageModule = Mn.Module.extend({
  startWithParent: false,
  initialize: function() {
    this.controller = new PageController();
    this.router = new PageRouter({
      controller: this.controller
    });
  },
  onStop: function() {
    this.controller.destroy();
  }
});

module.exports = PageModule;
