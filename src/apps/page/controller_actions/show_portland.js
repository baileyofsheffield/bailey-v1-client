var PageModel = require('../../../entities/page'),
  LoadingView = require('../../../common/loading'),
  LayoutView = require('../views/story/layout'),
  VideoView = require('../views/portland/video'),
  meta = require('../../../config/meta');

module.exports = function(self, region, slug) {
  region.show(new LoadingView());
  var page = new PageModel({slug: 'portland-works'});
  page.fetch().done(function(){
    var layoutView = new LayoutView({
      model: page,
      slug: page.get('slug')
    });
    self.listenTo(layoutView, 'show', function() {
      layoutView.linkRegion.show(new VideoView());
    });
    region.show(layoutView);
    var page_json = page.toJSON();
    meta(page_json.title, page_json.title, page_json.seo_description);
  });
};
