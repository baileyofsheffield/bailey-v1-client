var PageModel = require('../../../entities/page'),
  LoadingView = require('../../../common/loading'),
  LayoutView = require('../views/workshop/layout'),
  VideoView = require('../views/workshop/video'),
  meta = require('../../../config/meta');

module.exports = function(self, region, slug) {
  region.show(new LoadingView());
  var page = new PageModel({slug: 'workshop'});
  page.fetch().done(function(){
    var layoutView = new LayoutView({
      model: page
    });
    region.show(layoutView);
    var page_json = page.toJSON();
    meta(page_json.title, page_json.title, page_json.seo_description);
  });
};
