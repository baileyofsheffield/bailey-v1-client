var PageModel = require('../../../entities/page'),
  LoadingView = require('../../../common/loading'),
  PageView = require('../views/page/page'),
  meta = require('../../../config/meta');

module.exports = function(self, region, slug) {
  region.show(new LoadingView());
  var page = new PageModel({slug: slug});
  page.fetch().done(function(){
    region.show(new PageView({model: page}));
    var page_json = page.toJSON();
    meta(page_json.title, page_json.title, page_json.seo_description);
  });
};
