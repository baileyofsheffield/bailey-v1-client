var PageModel = require('../../../entities/page'),
  LoadingView = require('../../../common/loading'),
  LayoutView = require('../views/story/layout'),
  LinksView = require('../views/story/links'),
  meta = require('../../../config/meta'),
  globalChannel = require('../../../config/globalChannel');

module.exports = function(self, region, slug) {
  region.show(new LoadingView());
  var page = new PageModel({slug: 'story'});
  page.fetch().done(function(){
    var layoutView = new LayoutView({
      model: page
    });
    var linksView = new LinksView();
    self.listenTo(layoutView, 'show', function() {
      layoutView.linkRegion.show(linksView);
    });
    self.listenTo(linksView, 'portland:show', function() {
      globalChannel.trigger('portland:show');
    });
    self.listenTo(linksView, 'team:show', function() {
      globalChannel.trigger('team:show');
    });
    self.listenTo(linksView, 'sheffield:show', function() {
      globalChannel.trigger('sheffield:show');
    });
    self.listenTo(linksView, 'harry:show', function() {
      globalChannel.trigger('harry:show');
    });
    region.show(layoutView);
    var page_json = page.toJSON();
    meta(page_json.title, page_json.title, page_json.seo_description);
  });
};
