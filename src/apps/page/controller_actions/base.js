var app = require('../../../app'),
  globalChannel = require('../../../config/globalChannel');

module.exports = Mn.Object.extend({
  initialize: function() {
    return this.setHandlers();
  },
  setHandlers: function() {
    var _this = this;
    globalChannel.on('lookbook:list', function() {
      app.navigate("look-book");
      return _this.listLookBook();
    });
    globalChannel.on('story:show', function() {
      app.navigate('story');
      return _this.story();
    });
    globalChannel.on('portland:show', function() {
      app.navigate('portland-works');
      return _this.portland();
    });
    globalChannel.on('team:show', function() {
      app.navigate('meet-the-team');
      return _this.team();
    });
    globalChannel.on('sheffield:show', function() {
      app.navigate('our-sheffield');
      return _this.sheffield();
    });
    globalChannel.on('harry:show', function() {
      app.navigate('harry-stainless-steel');
      return _this.harry();
    });
    globalChannel.on('workshop:show', function() {
      app.navigate('workshop');
      return _this.workshop();
    });
    globalChannel.on('page:show', function(slug, category) {
      app.navigate(category + '/' + slug);
      return _this.page(slug);
    });
    globalChannel.on('review:list', function() {
      app.navigate('testimonials');
      return _this.review();
    });
  }
});
