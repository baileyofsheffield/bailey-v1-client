var LookBookCollection = require('../../../entities/lookbooks'),
  LoadingView = require('../../../common/loading'),
  LooksView = require('../views/list_lookbook/looks'),
  meta = require('../../../config/meta'),
  globalChannel = require('../../../config/globalChannel');

module.exports = function(self, region, options) {
  var loadingView = new LoadingView();
  region.show(loadingView);

  var looks = new LookBookCollection();

  looks.fetch().then(function() {

    var looksView = new LooksView({
      collection: looks
    });

    self.listenTo(looksView, 'childview:product:show', function (childview, model) {
      var product = model.get('product');
      if (product) globalChannel.trigger('shop:show', product.slug);
    });

    region.show(looksView);
    meta('Look Book', 'Look Book', 'The stainless steel CABLE™ bracelet: customise your style with beads and a choice of six striking PVD colours. Jewellery designed and hand assembled in Sheffield');
  });
};
