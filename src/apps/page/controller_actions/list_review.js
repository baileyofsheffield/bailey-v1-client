var ReviewCollection = require('../../../entities/reviews'),
  LoadingView = require('../../../common/loading'),
  ReviewView = require('../views/list_review/reviews'),
  meta = require('../../../config/meta'),
  globalChannel = require('../../../config/globalChannel');

module.exports = function(self, region, options) {
  region.show(new LoadingView());

  var reviews = new ReviewCollection();

  reviews.fetch().then(function() {
    var reviewView = new ReviewView({
      collection: reviews
    });

    region.show(reviewView);
  });
};
