var PageModel = require('../../../entities/page'),
  LoadingView = require('../../../common/loading'),
  LayoutView = require('../views/story/layout'),
  meta = require('../../../config/meta'),
  globalChannel = require('../../../config/globalChannel');

module.exports = function(self, region, slug) {
  region.show(new LoadingView());
  var page = new PageModel({slug: slug});
  page.fetch().done(function(){
    var layoutView = new LayoutView({
      model: page,
      slug: slug
    });
    region.show(layoutView);
    var page_json = page.toJSON();
    meta(page_json.title, page_json.title, page_json.seo_description);
  });
};
