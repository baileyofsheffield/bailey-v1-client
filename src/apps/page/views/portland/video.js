var VideoBehavior = require('../../../../behaviors/videoPlayer'),
  tpl = require('./templates/video.hbs');

module.exports = Mn.ItemView.extend({
  template: tpl,
  behaviors: {
    videoPlayer: {
      behaviorClass: VideoBehavior
    }
  }
});
