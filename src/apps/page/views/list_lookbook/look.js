var tpl = require('./templates/look.hbs');

module.exports = Mn.ItemView.extend({
  template: tpl,
  className: 'look-item',
  events: {
    'click .js-buy': 'productShow'
  },
  onShow: function () {
    var wide = this.model.get('wide');
    if (wide) {
      this.$el.addClass('look-item--width2');
    }
  },
  productShow: function () {
    this.trigger('product:show', this.model);
  }
});
