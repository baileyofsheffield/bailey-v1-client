var Packery = require('packery'),
  tpl = require('./templates/looks.hbs'),
  imagesLoaded = require('imagesloaded'),
  Look = require('./look');

module.exports = Mn.CompositeView.extend({
  template: tpl,
  className: "main-wrapper",
  childView: Look,
  childViewContainer: ".lookbook",
  onShow: function() {
    $(".lookbook").append("<div class='grid-sizer'></div><div class='gutter-sizer'></div>");
    var elem = document.querySelector('.lookbook');
    imagesLoaded(elem, function(instance) {
      var msnry = new Packery(elem, {
        itemSelector: '.look-item',
        percentPosition: true,
        columnWidth: '.grid-sizer',
        gutter: '.gutter-sizer'
      });
    });

  }
});
