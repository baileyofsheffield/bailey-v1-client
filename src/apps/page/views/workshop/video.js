var tpl = require('./templates/video.hbs');

module.exports = Mn.ItemView.extend({
  template: tpl,
  className: 'video-wrapper',
  events: {
    'click video': 'playVideo',
    'click .video-play': 'playVideo'
  },
  onShow: function () {
    $('#video').prop("controls", false);
  },
  playVideo: function (e) {
    e.preventDefault();
    $('#video').prop("controls", true);
    $('.video-play').hide();
    var video = document.getElementById("video");
    video.play();
  }
});
