var VideoBehavior = require('../../../../behaviors/videoPlayer'),
  tpl = require('./templates/layout.hbs');

module.exports = Mn.ItemView.extend({
  className: "story-layout main-wrapper",
  template: tpl,
  behaviors: {
    videoPlayer: {
      behaviorClass: VideoBehavior
    }
  }
});
