var tpl = require('./templates/reviews.hbs'),
  Review = require('./review');

module.exports = Mn.CompositeView.extend({
  template: tpl,
  className: "main-wrapper",
  childView: Review,
  childViewContainer: ".reviews"
});
