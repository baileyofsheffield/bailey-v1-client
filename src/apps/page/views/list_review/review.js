var tpl = require('./templates/review.hbs');

module.exports = Mn.ItemView.extend({
  template: tpl,
  className: 'review-item'
});
