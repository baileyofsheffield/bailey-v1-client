var tpl = require('./templates/story.hbs');

module.exports = Mn.ItemView.extend({
  template: tpl,
  tagName: 'article',
  className: 'story'
});
