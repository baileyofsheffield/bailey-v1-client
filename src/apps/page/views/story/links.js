var tpl = require('./templates/links.hbs');

module.exports = Mn.ItemView.extend({
  template: tpl,
  className: 'story-link-layout',
  events: {
    'click .portland-works': 'showPortland',
    'click .team': 'showTeam',
    'click .sheffield': 'showSheffield',
    'click .harry': 'showHarry'
  },
  showPortland: function (e) {
    e.preventDefault();
    this.trigger('portland:show');
  },
  showTeam: function (e) {
    e.preventDefault();
    this.trigger('team:show');
  },
  showSheffield: function (e) {
    e.preventDefault();
    this.trigger('sheffield:show');
  },
  showHarry: function (e) {
    e.preventDefault();
    this.trigger('harry:show');
  }
});
