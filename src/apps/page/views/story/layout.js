var VideoBehavior = require('../../../../behaviors/videoPlayer'),
  tpl = require('./templates/layout.hbs');

module.exports = Mn.LayoutView.extend({
  className: "story-layout main-wrapper",
  template: tpl,
  regions: {
    linkRegion: ".story-links"
  },
  serializeData: function() {
    var viewData = {
      page: this.model.toJSON(),
      slug: this.options.slug
    };
    return viewData;
  },
  behaviors: {
    videoPlayer: {
      behaviorClass: VideoBehavior
    }
  }
});
