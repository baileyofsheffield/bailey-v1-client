var BaseRouter = require('../../config/router');

module.exports = BaseRouter.extend({
  appRoutes: {
    "shop(/filter/:params)": "list",
    "shop/custom": "custom",
    "shop/:slug": "show"
  }
});
