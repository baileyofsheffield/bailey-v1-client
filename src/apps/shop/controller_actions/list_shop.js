var ProductDisplayCollection = require('../../../entities/productDisplays'),
  LoadingView = require('../../../common/loading'),
  LayoutView = require('../views/list/layout'),
  PanelView = require('../views/list/panel'),
  CableView = require('../views/list/cables'),
  BeadView = require('../views/list/beads'),
  AccessoryView = require('../views/list/accessories'),
  DecorationView = require('../views/list/decorations'),
  meta = require('../../../config/meta'),
  GA = require('../../../config/GA'),
  globalChannel = require('../../../config/globalChannel');

module.exports = function(self, region, options) {
  region.show(new LoadingView());
  var settings = globalChannel.request('current:settings');
  var cableProducts = new ProductDisplayCollection([], {
    parameters: options
  });

  var beadProducts = new ProductDisplayCollection([], {
    parameters: options
  });

  var accessoryProducts = new ProductDisplayCollection([], {
    parameters: options
  });

  var christmasProducts = new ProductDisplayCollection([], {
    parameters: options
  });

  var layoutView = new LayoutView({
    model: settings
  });
  var panelView = new PanelView();

  Promise.all([cableProducts.fetch({
    data: {
      product_type: '55dd9d33ffedd0cb162e38f7',
      customer: true
    }
  }), beadProducts.fetch({
    data: {
      product_type: '55dd9e6fffedd0cb162e38f8',
      customer: true
    }
  }), accessoryProducts.fetch({
    data: {
      product_type: '55dda683ffedd0cb162e38f9',
      customer: true
    }
  }), christmasProducts.fetch({
    data: {
      product_type: '5829e2c5b5619ff1502cf3fd',
      customer: true
    }
  })]).then(function() {

    var cableView = new CableView({
      collection: cableProducts
    });
    var beadView = new BeadView({
      collection: beadProducts
    });

    var accessoryView = new AccessoryView({
      collection: accessoryProducts
    });

    // var decorationView = new DecorationView({
    //   collection: christmasProducts
    // });


    self.listenTo(layoutView, 'show', function() {
      layoutView.panelRegion.show(panelView);
      layoutView.cableRegion.show(cableView);
      layoutView.beadRegion.show(beadView);
      layoutView.accessoryRegion.show(accessoryView);
      // layoutView.christmasRegion.show(decorationView);
    });

    self.listenTo(cableView, 'childview:product:show', function(childview, model) {
      var slug = model.get('slug');
      var product = model.toJSON();
      for (var i = 0; i < product.variations.length; i++) {
        var item = product.variations[i];
        dataLayer.push({
          'event': 'productClick',
          'ecommerce': {
            'click': {
              'actionField': {
                'list': 'Shop List'
              },
              'products': [{
                'id': item.sku,
                'name': product.title,
                'category': product.product_type.title,
                'variant': item.size || item.option_name,
                'position': childview._index
              }]
            }
          }
        });
      }
      if (slug === 'custom') {
        globalChannel.trigger('shop:custom');
      } else {
        globalChannel.trigger('shop:show', model.get('slug'));
      }
    });

    self.listenTo(beadView, 'childview:product:show', function(childview, model) {
      globalChannel.trigger('shop:show', model.get('slug'));
    });

    self.listenTo(accessoryView, 'childview:product:show', function(childview, model) {
      globalChannel.trigger('shop:show', model.get('slug'));
    });

    // self.listenTo(decorationView, 'childview:product:show', function(childview, model) {
    //   globalChannel.trigger('shop:show', model.get('slug'));
    // });

    region.show(layoutView);
    meta('Shop', 'Shop', 'Purchase your innovative stainless steel CABLE™ bracelet here - Unique jewellery invented, designed, manufactured and hand assembled in Sheffield, UK.');
    GA.listView(cableProducts.toJSON());
    GA.listView(beadProducts.toJSON());
    GA.listView(accessoryProducts.toJSON());
    GA.listView(christmasProducts.toJSON());
  }).catch(function(err) {
    return console.log(err);
  });
};
