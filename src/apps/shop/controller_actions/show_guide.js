var PageModel = require('../../../entities/page'),
  LoadingView = require('../../../common/loading_modal'),
  GuideView = require('../views/show/guide');

module.exports = function(self, region, slug) {
  region.show(new LoadingView());
  var page = new PageModel({slug: slug});
  page.fetch().done(function(){
    region.show(new GuideView({model: page}));
  });
};
