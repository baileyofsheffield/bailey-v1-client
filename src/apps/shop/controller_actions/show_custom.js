var LoadingView = require('../../../common/loading'),
  OptionCollection = require('../../../entities/customOptions'),
  CustomLineItemCollection = require('../../../entities/customLineItems'),
  ProductDisplayCollection = require('../../../entities/productDisplays'),
  EngravingCollection = require('../../../entities/engravings'),
  CustomLineItemModel = require('../../../entities/customLineItem'),
  ProductDisplayModel = require('../../../entities/productDisplay'),
  LayoutView = require('../views/custom/layout'),
  meta = require('../../../config/meta'),
  csrf = require('../../../config/csrf'),
  addCustom = require('../../../config/addCustom'),
  CustomView = require('../views/custom/view'),
  PriceView = require('../views/custom/price'),
  ProductView = require('../views/custom/product'),
  InfoView = require('../views/custom/info'),
  RecommendView = require('../views/recommend/recommends'),
  EngravingView = require('../views/show/engravings'),
  GA = require('../../../config/GA'),
  globalChannel = require('../../../config/globalChannel');

module.exports = function(self, region) {
  region.show(new LoadingView());
  var settings = globalChannel.request('current:settings');
  var product = new ProductDisplayModel({
    slug: 'custom'
  });
  var options = new OptionCollection();
  var items = new CustomLineItemCollection();
  var layoutView = new LayoutView({
    model: settings
  });
  var recommends = new ProductDisplayCollection();
  var engravings = new EngravingCollection();

  Promise.all([product.fetch({data: {customer: true}}), options.fetch(), items.fetch()]).then(function() {
    return engravings.fetch({
      data: {
        product: product.get('id')
      }
    });
  }).then(function() {
    if (items.length > 4 || items.length === 0) {
      window.localStorage.clear();
      items.reset();
    }
    var has_engravings = engravings.length > 0 ? true : false;
    var productJSON = product.toJSON();
    var variations = new Backbone.Collection(product.get('variations'));

    var customView = new CustomView({
      options: options,
      items: items,
      collection: engravings,
      has_engravings: has_engravings
    });

    var priceView = new PriceView({
      model: product,
      collection: items,
      engravings: engravings
    });

    var productView = new ProductView({
      model: product
    });

    var engravingView = new EngravingView({
      collection: engravings,
      product: product,
      settings: settings
    });

    var infoView = new InfoView({
      model: product
    });

    var recommendView = new RecommendView({
      collection: recommends
    });

    self.listenTo(layoutView, 'show', function() {
      layoutView.customRegion.show(customView);
      layoutView.productRegion.show(productView);
      layoutView.infoRegion.show(infoView);
    });

    self.listenTo(customView, 'show', function() {
      customView.priceRegion.show(priceView);
    });

    self.listenTo(customView, 'engraving:add', function() {
      if (layoutView.engravingRegion.hasView()) return engravingView.triggerMethod('open:panel');
      layoutView.engravingRegion.show(engravingView);
    });

    self.listenTo(customView, 'custom:option:selected', function(part, colour, colour_name, size) {
      var sku;
      if (size) {
        sku = part + '-' + colour + '-' + size;
      } else {
        sku = part + '-' + colour;
      }
      var option_product = variations.where({
        sku: sku
      });
      var data = {
        part: part,
        colour: colour,
        colour_name: colour_name,
        product: option_product[0].get('id'),
        total: option_product[0].get('price')
      };
      if (size) data.size = size;
      var current_item = items.where({
        part: part
      });
      var new_item;
      if (current_item.length > 0) {
        new_item = current_item[0];
      } else {
        new_item = new CustomLineItemModel();
      }
      new_item.save(data).then(function() {
        return items.add(new_item);
      });
    });

    self.listenTo(engravings, 'change:added sync', function() {
      items.trigger('sync');
    });

    self.listenTo(customView, 'cart:add', function() {
      if (layoutView.engravingRegion.hasView()) {
        engravingView.triggerMethod('close:panel');
      }
      var engraving_options;

      if (engravings.getAdded().length > 0) {
        engraving_options = new Backbone.Collection(engravings.chain().filter(function(item) {
          return item.get("added");
        }).invoke('toJSON').value());
      }
      addCustom(items.toJSON(), productJSON, engraving_options).then(function(resp) {
        globalChannel.trigger('basket:add', resp.body, resp.body.order);
        GA.addCart(productJSON, resp.body);
        return recommends.fetch({
          data: {
            recommend: productJSON.product_type.id
          }
        });
      }).then(function() {
        productView.triggerMethod('error');
        if (engravings.getAdded().length > 0) {
          engravings.reset();
          return engravings.fetch({
            data: {
              product: product.get('id')
            }
          });
        }
        return true;
      }).then(function() {
        return productView.recommendRegion.show(recommendView);
      }).then(function() {
        window.localStorage.clear();
        items.reset();
        customView.triggerMethod('reset');
      }).catch(function(err) {
        productView.triggerMethod('error');
      });
    });

    self.listenTo(customView, 'size:guide', function() {
      globalChannel.trigger('shop:size:guide', 'cable-bracelet-size-guide');
    });

    self.listenTo(recommendView, 'childview:product:show', function(childview, model) {
      var slug = model.get('slug');
      if (slug == 'custom') {
        globalChannel.trigger('shop:custom');
      } else {
        globalChannel.trigger('shop:show', model.get('slug'));
      }
    });

    self.listenTo(recommendView, 'cart:show', function() {
      globalChannel.trigger('cart:show');
    });
    self.listenTo(recommendView, 'shop:show', function() {
      globalChannel.trigger('shop:list');
    });
    region.show(layoutView);
    meta('Customise', 'Customise', 'Customise your stainless steel CABLE™ bracelet here - Jewellery to last more than a lifetime. Available in seven striking colours and finishes. Personalise your style.');
    GA.detailView(productJSON, 'Product Custom View');
  });
};
