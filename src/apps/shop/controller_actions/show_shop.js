var app = require('../../../app'),
	ProductDisplayModel = require('../../../entities/productDisplay'),
	ProductDisplayCollection = require('../../../entities/productDisplays'),
	EngravingCollection = require('../../../entities/engravings'),
	LoadingView = require('../../../common/loading'),
	LayoutView = require('../views/show/layout'),
	ProductView = require('../views/show/product'),
	InfoView = require('../views/show/info'),
	CartView = require('../views/show/cart'),
	RecommendView = require('../views/recommend/recommends'),
	EngravingView = require('../views/show/engravings'),
	addCart = require('../../../config/addCart'),
	meta = require('../../../config/meta'),
	message = require('../../../config/message'),
	GA = require('../../../config/GA'),
	globalChannel = require('../../../config/globalChannel');

module.exports = function(self, region, slug) {
	region.show(new LoadingView());
	var product = new ProductDisplayModel({
		slug: slug
	});
	var recommends = new ProductDisplayCollection();
	var engravings = new EngravingCollection();
	var settings = globalChannel.request('current:settings');
	product.fetch({data: {customer: true}}).then(function() {
		return engravings.fetch({
			data: {
				product: product.get('_id')
			}
		});
	}).then(function() {
		var has_engravings = engravings.length > 0 ? true : false;
		var product_json = product.toJSON();

		var layoutView = new LayoutView();

		var productView = new ProductView({
			model: product,
			collection: engravings,
			has_engravings: has_engravings,
			settings: settings
		});

		var recommendView = new RecommendView({
			collection: recommends
		});

		var engravingView = new EngravingView({
			collection: engravings,
			product: product,
			settings: settings
		});

		var infoView = new InfoView({
			model: product
		});

		self.listenTo(layoutView, 'show', function() {
			layoutView.productRegion.show(productView);
			layoutView.infoRegion.show(infoView);
		});
		self.listenTo(productView, 'cart:add', function(variation) {

      if (layoutView.engravingRegion.hasView()) {
				engravingView.triggerMethod('close:panel');
			}
			var engraving_options;
			if (engravings.getAdded().length > 0) {
				engraving_options = new Backbone.Collection(engravings.chain().filter(function(item) {
					return item.get("added");
				}).invoke('toJSON').value());
			}
			addCart(variation, product, engraving_options).then(function(resp) {
				globalChannel.trigger('basket:add', resp.body, resp.body.order);
				GA.addCart(product_json, resp.body);
				return recommends.fetch({
					data: {
						recommend: product_json.product_type.id
					}
				});
			}).then(function() {
				productView.triggerMethod('error');
				productView.triggerMethod('clear:selection');
				if (engravings.getAdded().length > 0) {
					engravings.reset();
					return engravings.fetch({
						data: {
							product: product.get('id')
						}
					});
				}
				return true;
			}).then(function () {
				if (productView.recommendRegion.hasView()) {
					return recommendView.triggerMethod('scroll:to');
				} else {
					return productView.recommendRegion.show(recommendView);
				}
			}).catch(function(err) {
				if (err && err.response) {
					message("error", window.t(err.response.body.error.message), 10000);
					return 	productView.triggerMethod('error');
				}
			});
		});

		self.listenTo(productView, 'size:guide', function() {
			var slug;
			if (product_json.product_type.title == 'Cable™') {
				globalChannel.trigger('shop:size:guide', 'cable-bracelet-size-guide');
			} else {
				globalChannel.trigger('shop:size:guide', 'pendant-converter-size-guide');
			}
		});

		self.listenTo(productView, 'engraving:add', function() {
			if (layoutView.engravingRegion.hasView()) return engravingView.triggerMethod('open:panel');
			layoutView.engravingRegion.show(engravingView);
		});

		self.listenTo(recommendView, 'childview:product:show', function(childview, model) {
			var slug = model.get('slug');
			if (slug == 'custom') {
				globalChannel.trigger('shop:custom');
			} else {
				globalChannel.trigger('shop:show', model.get('slug'));
			}
		});

		self.listenTo(recommendView, 'cart:show', function() {
			globalChannel.trigger('cart:show');
		});

		self.listenTo(recommendView, 'shop:show', function() {
			globalChannel.trigger('shop:list');
		});

		region.show(layoutView);

		var image;
		if (product_json.listing_image) {
			image = product_json.listing_image.public_id;
		}
		var product_title;
		if (product_json.product_type.title == "Cable™" || product_json.product_type.title == "Bead") {
			product_title = product_json.title + ' ' + product_json.product_type.title;
		} else {
			product_title = product_json.title;
		}
		meta(product_title, product_title, product_json.fields[0].seo_description, image);
		GA.detailView(product_json, 'Product Detail View');
	});
};
