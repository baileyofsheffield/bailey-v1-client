var app = require('../../../app'),
  globalChannel = require('../../../config/globalChannel');

var serializeParams = function(options) {
  options = _.pick(options, "material", "colour");
  return (_.map(_.filter(_.pairs(options), function(pair) {
    return pair[1];
  }), function(pair) {
    return pair.join(":");
  })).join("+");
};

module.exports = Mn.Object.extend({
  initialize: function() {
    return this.setHandlers();
  },
  setHandlers: function() {
    var _this = this;
    globalChannel.on('shop:custom', function() {
      app.navigate("shop/custom");
      return _this.custom();
    });
    globalChannel.on('shop:show', function(slug) {
      app.navigate("shop/" + slug);
      return _this.show(slug);
    });
    globalChannel.on('shop:list', function() {
      app.navigate("shop");
      return _this.list();
    });
    globalChannel.on('shop:filter', function(options) {
      app.navigate("shop/filter/" + serializeParams(options));
    });
    globalChannel.on('shop:size:guide', function(slug) {
      return _this.sizeGuide(slug);
    });
  }
});
