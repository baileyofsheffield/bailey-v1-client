var app = require('../../app'),
  ShopControllerBase = require('./controller_actions/base'),
  ShowShop = require('./controller_actions/show_shop'),
  ShowGuide = require('./controller_actions/show_guide'),
  ShowCustom = require('./controller_actions/show_custom'),
  ListShop = require('./controller_actions/list_shop'),
  globalChannel = require('../../config/globalChannel');

module.exports = ShopControllerBase.extend({
  show: function(slug) {
    app.startSubApp("ShopApp");
    ShowShop(this, app.main, slug);
  },
  custom: function() {
    app.startSubApp("ShopApp");
    ShowCustom(this, app.main);
    globalChannel.trigger("set:active:header", "shop/custom");
  },
  list: function(lang, options) {
    if (!options) {
      options = {
        page: 1
      };
    }
    app.startSubApp("ShopApp");
    ListShop(this, app.main, options);
    globalChannel.trigger("set:active:header", "shop");
  },
  sizeGuide: function(slug) {
    app.startSubApp("ShopApp");
    ShowGuide(this, app.modalRegion, slug);
  }
});
