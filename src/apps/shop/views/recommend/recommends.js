var owlCarousel = require('owl.carousel'),
  tpl = require('./templates/recommends.hbs'),
  Recommend = require('./recommend');

module.exports = Mn.CompositeView.extend({
  className: 'product-recommend-wrapper',
  childView: Recommend,
  childViewContainer: "div.recommend-carousel",
  template: tpl,
  triggers: {
    'click button.view-cart': 'cart:show',
    'click button.view-shop': 'shop:show'
  },
  onShow: function() {
    $('.product-recommend').addClass('show');
    $('.recommend-carousel').owlCarousel({
      items: 1,
      loop: true,
      nav: true,
      dots: false,
      responsive: {
        560: {
          items: 2
        },
        680: {
          items: 3
        },
        880: {
          items: 4
        },
        1064: {
          items: 5
        },
        1260: {
          items: 6
        }
      }
    });
    this.onScrollTo();
  },
  onScrollTo: function () {
    var scrollTop = $('.product-recommend').offset().top;
    var offset = $('.header').height();
    var custom = $('.custom-fixed').height();
    if (custom) {
      offset = offset + custom;
    }
    $('html, body').animate({
      scrollTop: scrollTop - offset
    }, 200, "linear");
  }
});
