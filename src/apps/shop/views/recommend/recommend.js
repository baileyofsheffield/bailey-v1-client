var tpl = require('./templates/recommend.hbs');

module.exports = Mn.ItemView.extend({
  template: tpl,
  className: 'product-item',
  events: {
    "click a.link": "linkClicked",
  },
  linkClicked: function(e){
    e.preventDefault();
    this.trigger("product:show", this.model);
  }
});
