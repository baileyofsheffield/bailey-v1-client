var tpl = require('./templates/product.hbs'),
  Tooltip = require('tether-tooltip'),
  VideoBehavior = require('../../../../behaviors/videoPlayer');

module.exports = Mn.LayoutView.extend({
  template: tpl,
  tagName: 'article',
  selectedProduct: null,
  events: {
    'click button.product-select': 'productSelected',
    'mouseenter button.add-cart': 'checkSelected',
    'mouseleave button.add-cart': 'closeTooltip',
    'click button.add-cart': 'addCart'
  },
  triggers: {
    'click a.js-size-guide': 'size:guide',
    'click a.js-engraving': 'engraving:add'
  },
  collectionEvents: {
    'change:added sync': 'render'
  },
  regions: {
    recommendRegion: ".product-recommend"
  },
  behaviors: {
    videoPlayer: {
      behaviorClass: VideoBehavior
    }
  },
  serializeData: function(){
    return {
      "item": this.model.toJSON(),
      "total": this.collection.getTotal(),
      "engraving_qty": this.collection.getAdded().length,
      "has_engravings": this.options.has_engravings,
      "settings": this.options.settings.toJSON()
    };
  },
  initialize: function() {
    this.variations = this.model.get('variations');
    if (this.variations.length == 1) {
      this.selectedProduct = this.variations[0]._id;
    }
  },
  onShow: function() {
    var that = this;
    this.addTooltip();
  },
  addTooltip: function () {
    if (this.variations.length > 1 && this.selectedProduct === null) {
      this.tooltip = new Tooltip({
        target: document.querySelector('.add-cart'),
        position: 'top center',
        content: "Please select a size.",
        classes: 'tooltip-theme-twipsy',
        remove: true
      });
    }
  },
  addCart: function() {
    if (this.selectedProduct) {
      this.trigger('cart:add', this.selectedProduct);
      this.$('.add-cart').prop('disabled', true).text('Adding...');
      this.closeTooltip();
    }
  },
  checkSelected: function() {
    if (this.tooltip && this.variations.length > 1 && this.selectedProduct === null) {
      this.tooltip.open();
    }
  },
  closeTooltip: function() {
    if (this.tooltip && this.variations.length > 1 && this.selectedProduct === null) {
      this.tooltip.close();
    }
  },
  productSelected: function(e) {
    e.preventDefault();
    this.$('.product-select').removeClass('selected');
    this.$(e.target).addClass('selected');
    this.selectedProduct = e.target.id;
    if (this.variations.length > 1 && this.tooltip) {
    	this.tooltip.destroy();
    	this.tooltip = null;
    }
  },
  onClearSelection: function() {
    this.$('.product-select').removeClass('selected');
    if (this.variations.length > 1) {
      this.selectedProduct = null;
      this.addTooltip();
    }
  },
  onError: function () {
    this.$('.add-cart').prop('disabled', false).text('Order now');
  },
  onDestroy: function() {
    if (this.fadeLogo) {
      this.fadeLogo.destroy();
    }
    $('body').removeClass("bar-menu");
  }
});
