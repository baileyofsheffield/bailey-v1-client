var tpl = require('./templates/guide.hbs');

module.exports = Mn.ItemView.extend({
  template: tpl,
  className: 'modal-underlay',
  events: {
    'click .close-model': 'modalClose'
  },
  modalClose: function () {
    this.trigger('modal:close');
  }
});
