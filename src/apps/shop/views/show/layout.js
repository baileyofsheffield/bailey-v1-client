var tpl = require('./templates/layout.hbs');

module.exports = Mn.LayoutView.extend({
  className: "product-layout",
  template: tpl,
  regions: {
    productRegion: ".product-content",
    infoRegion: ".product-info",
    engravingRegion: ".product-engraving"
  },
  onRender: function() {
    $('body').addClass('product-show');
  }
});
