var tpl = require('./templates/cart.hbs');

module.exports = Mn.ItemView.extend({
  template: tpl,
  className: 'md-content',
  triggers: {
    'click .md-continue': 'continue:shopping',
    'click .md-cart': 'view:cart'
  }
});
