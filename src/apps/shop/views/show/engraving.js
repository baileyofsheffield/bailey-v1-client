var baseViewMixins = require('../../../../mixins/baseViewMixin'),
  tpl = require('./templates/engraving.hbs');

var EngravingView = Mn.ItemView.extend(baseViewMixins);

module.exports = EngravingView.extend({
  tagName: "li",
  template: tpl,
  events: {
    'click .engraving-add': 'addEngraving',
    'click .engraving-edit': 'editEngraving',
    'click .engraving-delete': 'deleteEngraving'
  },
  computeds: {
    engravingTotal: function() {
      var text = this.getBinding('text'), price = this.getPrice();
			if (price > 0) {
				price = price / 100;
			}
      return '£' + price.toFixed(2);
    },
    createVisible: function() {
      return this.getBinding("editorOpen") &&
        !this.getBinding("editable");
    },
    editVisible: function() {
      return !this.getBinding("editorOpen") &&
        this.getBinding("editable");
    }
  },
  getPrice: function () {
    var product = this.options.product.toJSON();
    var engraving = this.model.toJSON();
    if (product.product_type.title == "Cable™" && engraving.title == "Clasp Left" || engraving.title == "Clasp Right" && this.options.settings.engraving_normal_price) {
      return this.options.settings.engraving_price;
    } else if (this.options.settings.engraving_normal_price) {
      return this.options.settings.engraving_normal_price;
    } else {
      return this.options.settings.engraving_price;
    }
  },
  addEngraving: function() {
    var text = this.$('#text').val();
    if (text.length === 0) return;
    var total = this.getPrice();
    this.model.set({
      text: text.toUpperCase(),
      total: total,
      editorOpen: false,
      editable: true,
      added: true
    });
  },
  editEngraving: function() {
    this.model.set({
      editorOpen: true,
      editable: false
    });
  },
  deleteEngraving: function() {
    this.model.set({
      text: '',
      total: 0,
      editorOpen: true,
      editable: false,
      added: false
    });
  }
});
