var Engraving = require('./engraving'),
  tpl = require('./templates/engravings.hbs');

module.exports = Mn.CompositeView.extend({
  template: tpl,
  childView: Engraving,
  childViewContainer: "ul",
  templateHelpers: function() {
    return {
      price: this.options.settings.get('engraving_price'),
      total: this.collection.getTotal()
    };
  },
  events: {
    'click .close-panel': 'onClosePanel'
  },
  collectionEvents: {
    'sync': 'render',
    'change:total': 'setTotal'
  },
  childViewOptions: function(model, index) {
    return {
      product: this.options.product,
      settings: this.options.settings.toJSON(),
      childIndex: index
    };
  },
  onShow: function () {
    this.onOpenPanel();
  },
  onClosePanel: function () {
    $('.product-engraving').removeClass('show');
  },
  onOpenPanel: function () {
    $('.product-engraving').addClass('show');
  },
  setTotal: function () {
    this.render();
    this.trigger('product:price:update', this.collection.getTotal());
  }
});
