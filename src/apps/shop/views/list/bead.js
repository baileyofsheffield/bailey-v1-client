var tpl = require('./templates/bead.hbs');

module.exports = Mn.ItemView.extend({
  tagName: "li",
  template: tpl,
  className: 'product-item',
  events: {
    "click a.link": "linkClicked",
  },

  linkClicked: function(e){
    e.preventDefault();
    this.trigger("product:show", this.model);
  }
});
