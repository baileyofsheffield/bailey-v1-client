var tpl = require('./templates/layout.hbs');

module.exports = Mn.LayoutView.extend({
  className: "shop-layout main-wrapper",
  template: tpl,
  regions: {
    filterRegion: ".product-filter",
    panelRegion: ".product-nav",
    cableRegion: ".cables",
    beadRegion: ".beads",
    accessoryRegion: ".accessories"
  },
  onRender: function() {
    $('body').addClass('shop');
  }
});
