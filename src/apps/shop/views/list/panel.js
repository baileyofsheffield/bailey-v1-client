var tpl = require('./templates/panel.hbs');

module.exports = Mn.ItemView.extend({
  template: tpl,
  tagName: 'ul',
  events: {
    'click a[href^="#"]': 'scrollTo'
  },
  scrollTo: function(e) {
    e.preventDefault();
    $('.product-nav li').removeClass('active');
    var target = e.target.hash;
    var $target = $(target);
    if ($target.length) {
      $('html, body').stop().animate({
        'scrollTop': $target.offset().top - 180
      }, 500, 'swing', function() {
        $(e.target).parent().addClass('active');
      });
    }

  }
});
