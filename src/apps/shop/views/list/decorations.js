var Decoration = require('./decoration');

module.exports = Mn.CollectionView.extend({
  tagName: "ul",
  childView: Decoration,
  onShow: function() {
    var hash = window.location.hash;
    if (hash) {
      setTimeout(function() {
        $('html, body').animate({
          scrollTop: $(hash).offset().top - 180
        });
      }, 300);
    }
  }
});
