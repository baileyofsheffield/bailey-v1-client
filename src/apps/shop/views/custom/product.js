var tpl = require('./templates/product.hbs');

module.exports = Mn.LayoutView.extend({
  template: tpl,
  tagName: 'article',
  regions: {
    recommendRegion: ".product-recommend"
  }
});
