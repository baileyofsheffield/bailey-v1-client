var tpl = require('./templates/layout.hbs');

module.exports = Mn.LayoutView.extend({
  className: "product-custom",
  template: tpl,
  regions: {
    customRegion: ".customiser",
    productRegion: ".product-content",
    infoRegion: ".product-info",
    engravingRegion: ".product-engraving"
  },
  onRender: function() {
    $('body').addClass('shop-custom');
  }
});
