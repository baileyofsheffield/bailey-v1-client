var tpl = require('./templates/custom.hbs');

module.exports = Mn.LayoutView.extend({
	template: tpl,
	selectedSize: null,
	selectedRopeColour: null,
	selectedRopeColourName: null,
	className: 'custom-header',
	events: {
		'click .change-cable': 'changeCable',
		'click .change-part': 'changePart',
		'click button.product-select': 'sizeSelected',
		'click button.add-cart': 'addCart'
	},
	triggers: {
		'click a.js-size-guide': 'size:guide',
		'click a.js-engraving': 'engraving:add'
	},
	regions: {
		priceRegion: ".price"
	},
	serializeData: function() {
		return {
			"options": this.options.options.toJSON(),
			"has_engravings": this.options.has_engravings
		};
	},
	onRender: function() {
		var that = this;
		var items = this.options.items;
		items.each(function(item) {
			var colour_name = item.get('colour_name');
			var part = item.get('part');
			var colour = item.get('colour');
			that.loadPart(part, colour);
			var size = item.get('size');
			if (size) {
				that.$('#' + size).addClass('selected');
				that.selectedSize = size;
				that.$('.custom-selectors').slideDown('fast');
			}
			if (part === 'ROPE') {
				that.selectedRopeColour = colour;
			}
			that.$('#' + part + '-' + colour).prop('checked', true);
			that.$('#' + part + '-WARNING').text(colour_name).removeClass('error');
		});
	},
	changeCable: _.debounce(function(e) {
		var colour = this.$(e.currentTarget).data('colour');
		var part = this.$(e.currentTarget).data('part');
		var colour_name = this.$(e.currentTarget).data('name');
		this.selectedRopeColour = colour;
		this.selectedRopeColourName = colour_name;
		this.$('#' + part).attr("fill", "url(#" + colour + ")");
		this.trigger('custom:option:selected', part, colour, colour_name, this.selectedSize);
		this.$('#' + part + '-' + colour).prop('checked', true);
		this.$('#' + part + '-WARNING').text(colour_name).removeClass('error');
	}, 500, true),
	changePart: _.debounce(function(e) {
		var colour = this.$(e.currentTarget).data('colour');
		var part = this.$(e.currentTarget).data('part');
		var colour_name = this.$(e.currentTarget).data('name');
		this.$('#' + part).attr("fill", "url(#" + colour + ")");
		this.trigger('custom:option:selected', part, colour, colour_name);
		this.$('#' + part + '-' + colour).prop('checked', true);
		this.$('#' + part + '-WARNING').text(colour_name).removeClass('error');
	}, 500, true),
	loadPart: function(part, colour) {
		this.$('#' + part).attr("fill", "url(#" + colour + ")");
	},
	sizeSelected: _.debounce(function(e) {
		e.preventDefault();
		this.$('.product-select').removeClass('selected');
		this.$(e.target).addClass('selected');
		this.selectedSize = e.target.id;
		if (this.selectedRopeColour) {
			this.trigger('custom:option:selected', 'ROPE', this.selectedRopeColour, this.selectedRopeColourName, this.selectedSize);
		}
		this.$('.custom-selectors').slideDown('fast');
	}, 500, true),
	addCart: function() {
		$('.custom-warning').text('').hide();
		var rope = $('input[name="ROPE"]').is(":checked");
		if (!rope) $('.rope-warning').text('Please select a rope colour').show();
		var left_clasp = $('input[name="CLASP-LEFT"]').is(":checked");
		if (!left_clasp) $('.left-clasp-warning').text('Please select a colour').show();
		var middle_clasp = $('input[name="CLASP-MIDDLE"]').is(":checked");
		if (!middle_clasp) $('.middle-clasp-warning').text('Please select a colour').show();
		var right_clasp = $('input[name="CLASP-RIGHT"]').is(":checked");
		if (!right_clasp) $('.right-clasp-warning').text('Please select a colour').show();
		if (!rope || !left_clasp || !middle_clasp || !right_clasp) return;
		if (this.options.items.length < 4) return;
		var data = Backbone.Syphon.serialize(this);
		this.trigger('cart:add');
		this.$('.add-cart').prop('disabled', true).text('Adding...');
	},
	onReset: function () {
		this.$('.product-select').removeClass('selected');
		this.$('.custom-colour').prop('checked', false);
		this.$('.add-cart').prop('disabled', false).text('Order Now');
		that.$('.custom-selectors').slideUp('fast');
	}
});
