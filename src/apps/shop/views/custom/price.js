var tpl = require('./templates/price.hbs');
var waitForPause, pauseDelay = 1000;
module.exports = Mn.ItemView.extend({
  template: tpl,
  serializeData: function() {
    var total = this.collection.reduce(function(memo, value) {
      return memo + value.get("total");
    }, 0);
    return {
      "product": this.options.model.toJSON(),
      "total": total,
      "engraving_total": this.options.engravings.getTotal(),
      "engraving_qty": this.options.engravings.getAdded().length
    };
  },
  collectionEvents: {
    'add sync reset': 'render'
  },
  onShow: function () {
    this.setHeight();
  },
  setHeight: function () {
    var header = $('.header').height();
    var height = $('.custom-fixed').height();
    var new_height = height + header - 20;
    $(".custom-options").css("margin-top", new_height + "px");
  }
});
