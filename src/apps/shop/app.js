var app = require('../../app'),
  ShopRouter = require('./router'),
  ShopController = require('./controller'),
  ShopModule;

ShopModule = Mn.Module.extend({
  startWithParent: false,
  initialize: function() {
    this.controller = new ShopController();
    this.router = new ShopRouter({
      controller: this.controller
    });
  },
  onStop: function() {
    this.controller.destroy();
  }
});

module.exports = ShopModule;
