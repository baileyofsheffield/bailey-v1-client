var tpl = require('./templates/menu.hbs'),
    Menu = require('./menu_item');

module.exports = Mn.CompositeView.extend({
  template: tpl,
  tagName: 'nav',
  childView: Menu,
  childViewContainer: "ul",
  onRender: function() {
    var heading = this.options.heading;
    if (heading) {
      this.$('ul').before('<h3>' + heading + '</h3>');
    }
  }
});
