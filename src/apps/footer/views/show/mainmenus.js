var tpl = require('./templates/mainmenu.hbs'),
    Menu = require('./menu_item');

module.exports = Mn.CompositeView.extend({
  template: tpl,
  tagName: 'nav',
  childView: Menu,
  childViewContainer: "ul.menu"
});
