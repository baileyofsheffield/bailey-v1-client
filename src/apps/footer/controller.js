var MenusView = require('./views/show/menus'),
  MainMenusView = require('./views/show/mainmenus'),
  MenuCollection = require('../../entities/menus'),
  MainMenu = require('../../config/menus/mainMenu'),
  YourMenu = require('../../config/menus/yourMenu'),
  TechMenu = require('../../config/menus/techMenu'),
  PolicyMenu = require('../../config/menus/policyMenu'),
  globalChannel = require('../../config/globalChannel'),
  menus;

module.exports = Mn.Object.extend({
  show: function() {
    var self = this;
    var footerMainMenu = this.options.footerMainMenu;
    var footerYourMenu = this.options.footerYourMenu;
    var footerTechMenu = this.options.footerTechMenu;
    var footerPolicyMenu = this.options.footerPolicyMenu;
    var desktopMenu = _.filter(MainMenu, function(menu) {
      return menu.desktop === true;
    });
    var mainMenu = new MenuCollection(desktopMenu);
    var yourMenu = new MenuCollection(YourMenu);
    var techMenu = new MenuCollection(TechMenu);
    var policyMenu = new MenuCollection(PolicyMenu);

    var mainMenuView = new MainMenusView({
      collection: mainMenu
    });

    var yourMenuView = new MenusView({
      collection: yourMenu,
      heading: 'Bailey Essentials'
    });

    var techMenuView = new MenusView({
      collection: techMenu,
      heading: 'Technical guides'
    });

    var policyMenuView = new MenusView({
      collection: policyMenu,
      heading: 'Policy'
    });

    self.listenTo(mainMenuView, "childview:navigate", function(childView, navigationTrigger, slug) {
      globalChannel.trigger(navigationTrigger, slug);
    });

    self.listenTo(techMenuView, "childview:navigate", function(childView, navigationTrigger, slug, category) {
      globalChannel.trigger(navigationTrigger, slug, category);
    });

    self.listenTo(yourMenuView, "childview:navigate", function(childView, navigationTrigger, slug, category) {
      globalChannel.trigger(navigationTrigger, slug, category);
    });

    self.listenTo(policyMenuView, "childview:navigate", function(childView, navigationTrigger, slug, category) {
      globalChannel.trigger(navigationTrigger, slug, category);
    });

    footerMainMenu.show(mainMenuView);
    footerYourMenu.show(yourMenuView);
    footerTechMenu.show(techMenuView);
    footerPolicyMenu.show(policyMenuView);
  }
});
