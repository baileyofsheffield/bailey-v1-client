var app = require('../../app'),
  FooterController = require('./controller'),
  globalChannel = require('../../config/globalChannel'),
  FooterModule;

FooterModule = Mn.Module.extend({
  startWithParent: false,
  onStart: function() {
    this.controller = new FooterController({
      footerMainMenu: this.app.footerMainMenu,
      footerYourMenu: this.app.footerYourMenu,
      footerTechMenu: this.app.footerTechMenu,
      footerPolicyMenu: this.app.footerPolicyMenu
    });
    this.controller.show();
  },
  onStop: function() {
    this.controller.destroy();
  }
});

module.exports = FooterModule;
