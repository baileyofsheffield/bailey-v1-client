var app = require('../../app'),
  UserControllerBase = require('./controller_actions/base'),
  ShowUser = require('./controller_actions/show_user'),
  EditUser = require('./controller_actions/edit_user'),
  ShowOrder = require('./controller_actions/show_order'),
  ListOrder = require('./controller_actions/list_order'),
  ListProduct = require('./controller_actions/list_product'),
  RegisterProduct = require('./controller_actions/register_product');

module.exports = UserControllerBase.extend({
  show: function(userid) {
    app.startSubApp("UserApp");
    ShowUser(this, app.main, userid);
  },
  products: function(userid) {
    app.startSubApp("UserApp");
    ListProduct(this, app.main, userid);
  },
  edit: function(userid) {
    app.startSubApp("UserApp");
    EditUser(this, app.main, userid);
  },
  order: function(userid, orderid) {
    app.startSubApp("UserApp");
    ShowOrder(this, app.main, userid, orderid);
  },
  orders: function(userid) {
    app.startSubApp("UserApp");
    ListOrder(this, app.main, userid);
  },
  cableRegistration: function(token) {
    app.startSubApp("UserApp");
    $('body').addClass('bg-grey');
    RegisterProduct(this, app.main, token);
  }
});
