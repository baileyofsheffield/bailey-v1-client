var tpl = require('./templates/layout.hbs');

module.exports = Mn.LayoutView.extend({
  template: tpl,
  regions: {
    introRegion: ".register-intro",
    productRegion: ".registered-products"
  }
});
