var tpl = require('./templates/list_item.hbs');

module.exports = Mn.ItemView.extend({
  tagName: "tr",
  template: tpl,
  events: {
    "click a.js-show": "showOrder"
  },
  showOrder: function() {
    this.trigger("user:order:show", this.model.get('orderid'));
  }
});
