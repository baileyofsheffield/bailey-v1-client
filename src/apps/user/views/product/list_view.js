var ItemView = require('./item_view'),
  tpl = require('./templates/list.hbs');

module.exports = Mn.CompositeView.extend({
  template: tpl,
  childView: ItemView,
  childViewContainer: "tbody"
});
