var tpl = require('./templates/layout.hbs');

module.exports = Mn.LayoutView.extend({
  className: "main-wrapper",
  template: tpl,
  regions: {
    menuRegion: ".user-account-menu",
    accountRegion: ".user-account"
  },
  onShow: function () {
    $('body').addClass('bg-grey');
  }
});
