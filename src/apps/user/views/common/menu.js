var tpl = require('./templates/menu.hbs');

module.exports = Mn.ItemView.extend({
  tagName: 'li',
  template: tpl,
  events: {
    "click a": "navigate"
  },
  navigate: function(e) {
    e.preventDefault();
    this.trigger("navigate", this.model.get('navigationTrigger'));
  },
  onRender: function() {
    if (this.model.selected) {
      this.$el.addClass("active");
    }
  }
});
