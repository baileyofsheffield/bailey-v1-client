var FormBehavior = require('../../../../behaviors/formValidate'),
  tpl = require('./templates/confirm.hbs');

module.exports = Mn.ItemView.extend({
  template: tpl,
  className: 'main-wrapper',
  behaviors: {
    formValidate: {
      behaviorClass: FormBehavior
    }
  }
});
