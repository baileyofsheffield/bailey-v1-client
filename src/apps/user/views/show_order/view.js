var tpl = require('./templates/view.hbs');

module.exports = Mn.ItemView.extend({
  template: tpl,
  className: 'user-order',
  serializeData: function() {
    return {
      order: this.options.model.toJSON()
    };
  },
});
