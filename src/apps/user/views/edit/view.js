var FormBehavior = require('../../../../behaviors/formValidate'),
  tpl = require('./templates/view.hbs');

module.exports = Mn.ItemView.extend({
  template: tpl,
  tagName: 'form',
  id: 'update-account',
  behaviors: {
    formValidate: {
      behaviorClass: FormBehavior
    }
  }
});
