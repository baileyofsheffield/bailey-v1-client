var app = require('../../app'),
  UserRouter = require('./router'),
  UserController = require('./controller'),
  UserModule;

UserModule = Mn.Module.extend({
  startWithParent: false,
  initialize: function() {
    this.controller = new UserController();
    this.router = new UserRouter({
      controller: this.controller
    });
  },
  onStop: function() {
    this.controller.destroy();
  }
});

module.exports = UserModule;
