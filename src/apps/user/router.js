var BaseRouter = require('../../config/router');

module.exports = BaseRouter.extend({
	appRoutes: {
		"user/:userid": "show",
		"user/:userid/registered": "products",
		"user/:userid/edit": "edit",
		"user/:userid/orders": "orders",
		"user/:userid/order/:orderid": "order",
		"cable/registration/:token": "cableRegistration"
	}
});
