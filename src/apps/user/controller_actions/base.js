var app = require('../../../app'),
  globalChannel = require('../../../config/globalChannel');

module.exports = Mn.Object.extend({
  initialize: function() {
    return this.setHandlers();
  },
  setHandlers: function() {
    var _this = this;
    globalChannel.on('user:show', function(userid) {
      app.navigate("user/" + userid);
      return _this.show(userid);
    });
    globalChannel.on('user:edit', function(userid) {
      app.navigate("user/" + userid + '/edit');
      return _this.edit(userid);
    });
    globalChannel.on('user:order:show', function(userid, orderid) {
      app.navigate("user/" + userid + "/order/" + orderid);
      return _this.order(userid, orderid);
    });
    globalChannel.on('user:order:list', function(userid) {
      app.navigate("user/" + userid + "/orders");
      return _this.orders(userid);
    });
    globalChannel.on('user:logout', function(userid) {
      globalChannel.trigger('basket:reset');
      window.location.href = '/api/logout';
    });
    globalChannel.on('user:product:register', function(userid) {
      app.navigate("user/" + userid + "/registered");
      return _this.products(userid);
    });
  }
});
