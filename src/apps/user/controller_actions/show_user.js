var UserModel = require('../../../entities/user'),
  MenuCollection = require('../../../entities/menus'),
  AccountMenu = require('../../../config/menus/accountMenu'),
  LoadingView = require('../../../common/loading'),
  LayoutView = require('../views/common/layout'),
  MenuView = require('../views/common/menus'),
  UserView = require('../views/show/view'),
  csrf = require('../../../config/csrf'),
  globalChannel = require('../../../config/globalChannel'),
  userChannel = require('../../../config/userChannel');

module.exports = function(self, region, userid) {
  region.show(new LoadingView());
  var menus = new MenuCollection(AccountMenu);
  var user = new UserModel({
    userid: userid
  });

  var layoutView = new LayoutView();

  user.fetch().then(function() {
    
    var menuView = new MenuView({
      collection: menus
    });

    var userView = new UserView({
      model: user
    });

    self.listenTo(layoutView, 'show', function() {
      layoutView.menuRegion.show(menuView);
      layoutView.accountRegion.show(userView);
    });

    self.listenTo(menuView, "childview:navigate", function(childView, navigationTrigger) {
      globalChannel.trigger(navigationTrigger, userid);
    });

    region.show(layoutView);
  });

};
