var OrderCollection = require('../../../entities/orders'),
  MenuCollection = require('../../../entities/menus'),
  AccountMenu = require('../../../config/menus/accountMenu'),
  LoadingView = require('../../../common/loading'),
  LayoutView = require('../views/common/layout'),
  MenuView = require('../views/common/menus'),
  OrderView = require('../views/list_order/list_view'),
  csrf = require('../../../config/csrf'),
  globalChannel = require('../../../config/globalChannel'),
  userChannel = require('../../../config/userChannel');

module.exports = function(self, region, userid) {
  region.show(new LoadingView());
  var menus = new MenuCollection(AccountMenu);
  var orders = new OrderCollection();

  var layoutView = new LayoutView();

  orders.accountOrders().then(function() {
    var menuView = new MenuView({
      collection: menus
    });
    var orderView = new OrderView({
      collection: orders
    });
    self.listenTo(layoutView, 'show', function() {
      layoutView.menuRegion.show(menuView);
      layoutView.accountRegion.show(orderView);
    });

    self.listenTo(orderView, "childview:user:order:show", function(childView, orderid) {
      globalChannel.trigger("user:order:show", userid, orderid);
    });

    self.listenTo(menuView, "childview:navigate", function(childView, navigationTrigger) {
      globalChannel.trigger(navigationTrigger, userid);
    });

    region.show(layoutView);
  });

};
