var SerialCollection = require('../../../entities/serials'),
	MenuCollection = require('../../../entities/menus'),
	AccountMenu = require('../../../config/menus/accountMenu'),
	LoadingView = require('../../../common/loading'),
	LayoutView = require('../views/common/layout'),
	MenuView = require('../views/common/menus'),
	RegisterView = require('../views/product/layout'),
	ProductView = require('../views/product/list_view'),
	IntroView = require('../views/product/intro'),
	csrf = require('../../../config/csrf'),
	globalChannel = require('../../../config/globalChannel'),
	userChannel = require('../../../config/userChannel');

module.exports = function(self, region, userid) {
	region.show(new LoadingView());
	var menus = new MenuCollection(AccountMenu);
	var serials = new SerialCollection();

	var layoutView = new LayoutView();

	var menuView = new MenuView({
		collection: menus
	});

	var registerView = new RegisterView();

	var introView = new IntroView();
	serials.fetch().then(function () {

		var productView = new ProductView({
			collection: serials
		});

		self.listenTo(layoutView, 'show', function() {
			layoutView.menuRegion.show(menuView);
			layoutView.accountRegion.show(registerView);
		});

		self.listenTo(registerView, 'show', function() {
			registerView.introRegion.show(introView);
			registerView.productRegion.show(productView);
		});

		self.listenTo(menuView, "childview:navigate", function(childView, navigationTrigger) {
			globalChannel.trigger(navigationTrigger, userid);
		});

		region.show(layoutView);
	});
};
