var OrderModel = require('../../../entities/order'),
    ItemsCollection = require('../../../entities/lineitems'),
    MenuCollection = require('../../../entities/menus'),
    AccountMenu = require('../../../config/menus/accountMenu'),
    LoadingView = require('../../../common/loading'),
    LayoutView = require('../views/common/layout'),
    MenuView = require('../views/common/menus'),
    OrderView = require('../views/show_order/view'),
    csrf = require('../../../config/csrf'),
    globalChannel = require('../../../config/globalChannel'),
    userChannel = require('../../../config/userChannel');

module.exports = function(self, region, userid, orderid) {
    region.show(new LoadingView());
    var menus = new MenuCollection(AccountMenu);

    var order = new OrderModel({
        orderid: orderid
    });

    var items = new ItemsCollection();

    var layoutView = new LayoutView();

    order.fetchAccount().then(function() {
        var menuView = new MenuView({
            collection: menus
        });
        var orderView = new OrderView({
            model: order
        });
        self.listenTo(layoutView, 'show', function() {
            layoutView.menuRegion.show(menuView);
            layoutView.accountRegion.show(orderView);
        });

        self.listenTo(menuView, "childview:navigate", function(childView, navigationTrigger) {
            globalChannel.trigger(navigationTrigger, userid);
        });

        region.show(layoutView);

    });

};
