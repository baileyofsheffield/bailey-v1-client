var LoadingView = require('../../../common/loading'),
  RegisterView = require('../views/register/confirm'),
  SerialCheck = require('../../../entities/serialCheck'),
  message = require('../../../config/message'),
  csrf = require('../../../config/csrf'),
  userChannel = require('../../../config/userChannel'),
  globalChannel = require('../../../config/globalChannel');

module.exports = function(self, region, token) {
  var user = userChannel.request('current:user');
  region.show(new LoadingView());
  if (!user.get('auth')) {
    return globalChannel.trigger('login:show');
  }
  var serialCheck = new SerialCheck();
  var registerView = new RegisterView({model: serialCheck});
  self.listenTo(registerView, 'form:submit', function (data) {
    data.token = token;
    csrf().then(function () {
      return serialCheck.save(data);
    }).then(function (resp) {
      if(serialCheck.isValid()) {
        message("notice", "Your CABLE™ bracelet is now registered");
        globalChannel.trigger('user:product:register', user.get('userid'));
      }
    }).catch(function(resp) {
      message("error", resp.responseText);
      registerView.triggerMethod('button:reset');
    });
  });

  region.show(registerView);

};
