var FormBehavior = require('../../../../behaviors/formValidate'),
  tpl = require('./templates/create.hbs');

module.exports = Mn.ItemView.extend({
  template: tpl,
  tagName: 'form',
  id: 'create-form',
  behaviors: {
    formValidate: {
      behaviorClass: FormBehavior
    }
  }
});
