var FormBehavior = require('../../../../behaviors/formValidate'),
  tpl = require('./templates/login.hbs');

module.exports = Mn.ItemView.extend({
  template: tpl,
  tagName: 'form',
  id: 'login-form',
  events: {
    'keyup input.form-input': 'inputUpdate',
    'blur input.form-input': 'inputUpdate',
    'change input.form-input': 'inputUpdate',
    "click a.forgot-password": "forgotClicked"
  },
  behaviors: {
    formValidate: {
      behaviorClass: FormBehavior
    }
  },
  onShow: function () {
    $('body').addClass('bg-grey');
  },
  inputUpdate: function(e) {
    e.preventDefault();
    var value = $(e.target).val();
    if (value.length > 0) {
      $(e.target).closest("span.input").addClass('has-input');
    } else {
      $(e.target).closest("span.input").removeClass('has-input');
    }
  },
  forgotClicked: function(e) {
    e.preventDefault();
    this.trigger("forgot:show");
  }
});
