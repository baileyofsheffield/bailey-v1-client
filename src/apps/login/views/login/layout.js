var tpl = require('./templates/layout.hbs');

module.exports = Mn.LayoutView.extend({
  template: tpl,
  className: 'account-login main-wrapper',
  regions: {
    loginRegion: ".user-login",
    createRegion: ".create-account"
  }
});
