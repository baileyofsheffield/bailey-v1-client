var LoginRouter = require('./router'),
  LoginController = require('./controller');

module.exports = Mn.Module.extend({
  startWithParent: false,
  initialize: function() {
    this.controller = new LoginController();
    this.router = new LoginRouter({
      controller: this.controller
    });
  },
  onStop: function() {
    this.controller.destroy();
  }
});
