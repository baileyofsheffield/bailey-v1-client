module.exports = Mn.AppRouter.extend({
  appRoutes: {
    "login": "login",
    "forgot": "forgot",
    "reset/:token": "reset"
  }
});
