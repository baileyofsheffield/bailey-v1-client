var LoadingView = require('../../../common/loading'),
  ForgotView = require('../views/forgot/forgot'),
  PasswordForgot = require('../../../entities/forgot'),
  message = require('../../../config/message'),
  csrf = require('../../../config/csrf'),
  userChannel = require('../../../config/userChannel'),
  globalChannel = require('../../../config/globalChannel');

module.exports = function(self, region) {
  var user = userChannel.request('current:user');
  region.show(new LoadingView());
  if (user.get('auth')) {
    return globalChannel.trigger('home:show');
  }
  var forgotModel = new PasswordForgot();
  var forgotView = new ForgotView({
    model: forgotModel
  });

  self.listenTo(forgotView, 'form:submit', function(data) {
    csrf().then(function () {
      return forgotModel.save(data);
    }).then(function (resp) {
      if(forgotModel.isValid()) {
        if (resp) message('notice', window.t(resp.message), 10000);
        forgotView.triggerMethod('button:reset');
        return globalChannel.trigger('forgot:show');
      }
    }).catch(function(resp) {
      forgotView.triggerMethod('button:reset');
      if (resp && resp.responseJSON) message('error', window.t(resp.responseJSON.message), 10000);
    });
  });

  region.show(forgotView);

};
