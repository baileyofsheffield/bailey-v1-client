var LoadingView = require('../../../common/loading'),
  ResetView = require('../views/reset/reset'),
  PasswordReset = require('../../../entities/reset'),
  message = require('../../../config/message'),
  csrf = require('../../../config/csrf'),
  userChannel = require('../../../config/userChannel'),
  globalChannel = require('../../../config/globalChannel');

module.exports = function(self, region, token) {
  var user = userChannel.request('current:user');
  region.show(new LoadingView());
  if (user.get('auth')) {
    return globalChannel.trigger('home:show');
  }
  var resetModel = new PasswordReset();
  var resetView = new ResetView({model: resetModel});
  self.listenTo(resetView, 'form:submit', function (data) {
    data.token = token;
    csrf().then(function () {
      return resetModel.save(data);
    }).then(function (resp) {
      if(resetModel.isValid()) {
        if (resp) message('notice', window.t(resp.message), 10000);
        resetView.triggerMethod('button:reset');
        return globalChannel.trigger('login:show');
      }
    }).catch(function(resp) {
      if (resp && resp.responseJSON) message('error', window.t(resp.responseJSON.error.message), 10000);
      resetView.triggerMethod('button:reset');
      return globalChannel.trigger('forgot:show');
    });
  });

  region.show(resetView);

};
