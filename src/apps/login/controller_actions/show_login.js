var LoadingView = require('../../../common/loading'),
  LayoutView = require('../views/login/layout'),
  LoginView = require('../views/login/login'),
  CreateView = require('../views/login/create'),
  UserLogin = require('../../../entities/login'),
  UserModel = require('../../../entities/user'),
  message = require('../../../config/message'),
  Cookies = require('cookies-js'),
  csrf = require('../../../config/csrf'),
  userChannel = require('../../../config/userChannel'),
  globalChannel = require('../../../config/globalChannel');

module.exports = function(self, region) {
  var user = userChannel.request('current:user');
  region.show(new LoadingView());
  if (user.get('auth')) {
    return globalChannel.trigger('home:show');
  }
  region.show(new LoadingView());
  var createUser = new UserModel();
  var userLogin = new UserLogin();

  var loginView = new LoginView({
    model: userLogin
  });

  var createView = new CreateView({
    model: createUser
  });

  var layoutView = new LayoutView();

  self.listenTo(layoutView, 'show', function() {
    layoutView.loginRegion.show(loginView);
    layoutView.createRegion.show(createView);
  });

  self.listenTo(loginView, 'form:submit', function(data) {
    csrf().then(function() {
      return userLogin.save(data);
    }).then(function(resp) {
      if (userLogin.isValid()) {
        userChannel.trigger('set:current:user', resp);
        loginView.triggerMethod('button:reset');
        window.history.back();
      }
    }).catch(function(resp) {
      loginView.triggerMethod('button:reset');
      if (resp && resp.responseJSON) message('error', window.t(resp.responseJSON.message), 10000);
    });
  });

  self.listenTo(createView, 'form:submit', function(data) {
    csrf().then(function() {
      return createUser.signup(data);
    }).then(function(resp) {
      if (createUser.isValid()) {
        userChannel.trigger('set:current:user', resp);
        createView.triggerMethod('button:reset');
        window.history.back();
        fbq('track', 'CompleteRegistration');
        if (data.newsletter) {
          Cookies.set('viewedOuibounceModal', true, { expires: 1296000 });
        }
      }
    }).catch(function(resp) {
      createView.triggerMethod('button:reset');
      if (resp && resp.responseJSON) message('error', window.t(resp.responseJSON.message), 10000);
    });
  });

  self.listenTo(loginView, 'forgot:show', function(data) {
    globalChannel.trigger('forgot:show');
  });

  region.show(layoutView);

};
