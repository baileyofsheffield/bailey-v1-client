var app = require('../../../app'),
  globalChannel = require('../../../config/globalChannel');

module.exports = Mn.Object.extend({
  initialize: function() {
    return this.setHandlers();
  },
  setHandlers: function() {
    var _this = this;
    globalChannel.on('login:show', function() {
      app.navigate("login");
      return _this.login();
    });
    globalChannel.on('forgot:show', function() {
      app.navigate("forgot");
      return _this.forgot();
    });
  }
});
