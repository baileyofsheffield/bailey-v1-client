var app = require('../../app'),
  LoginControllerBase = require('./controller_actions/base'),
  ShowLogin = require('./controller_actions/show_login'),
  ShowReset = require('./controller_actions/show_reset'),
  ShowForgot = require('./controller_actions/show_forgot'),
  globalChannel = require('../../config/globalChannel');

module.exports = LoginControllerBase.extend({
  login: function() {
    app.startSubApp("LoginApp");
    ShowLogin(this, app.main);
    $('body').addClass('bg-grey');
  },
  reset: function(token) {
    app.startSubApp("LoginApp");
    ShowReset(this, app.main, token);
    $('body').addClass('bg-grey');
  },
  forgot: function() {
    app.startSubApp("LoginApp");
    ShowForgot(this, app.main);
    $('body').addClass('bg-grey');
  }
});
