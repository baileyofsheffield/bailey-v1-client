var tpl = require('./templates/account.hbs');

module.exports = Mn.ItemView.extend({
  tagName: 'div',
  template: tpl,
  modelEvent: {
    'change': 'render'
  },
  triggers: {
    'click .show-login': 'login:show'
  },
  events: {
    'click .mobile-menu-icon': 'showMobileMenu',
    'click .show-account': 'showAccount'
  },
  showMobileMenu: function (e) {
    e.preventDefault();
    $('body').toggleClass('show-mobile-menu');
  },
  showAccount: function (e) {
    e.preventDefault();
    this.trigger('user:show', this.model);
  }
});
