var tpl = require('./templates/cart.hbs');

module.exports = Mn.ItemView.extend({
  tagName: 'ul',
  className: 'cart-menu',
  template: tpl,
  collectionEvents: {
    'sync reset remove add': 'render'
  },
  modelEvents: {
    'change': 'render'
  },
  events: {
    'click a.view-bag': 'viewCart'
  },
  serializeData: function() {
    var qty = this.collection.reduce(function(memo, value) {
      return memo + value.get("qty");
    }, 0);
    var total = this.options.total.get('total') || 0;
    return {
      total: total,
      qty: qty
    };
  },
  viewCart: function(e) {
    e.preventDefault();
    this.trigger('cart:show');
  }
});
