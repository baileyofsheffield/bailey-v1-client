var tpl = require('./templates/menuItem.hbs');

module.exports = Mn.ItemView.extend({
  tagName: 'li',
  className: 'menu-item',
  template: tpl,
  events: {
    "click a": "navigate"
  },
  navigate: function(e) {
    // e.preventDefault();
    // this.trigger("navigate", this.model.get('navigationTrigger'), this.model.get('url'));
    $('body').removeClass('show-mobile-menu');
  },
  onRender: function() {
    var has_class = this.model.get('class');
    if (has_class) {
      this.$el.addClass(has_class);
    }
    if (this.model.selected) {
      this.$el.addClass("active");
    }
  }
});
