var Menu = require('./menu_item');

module.exports = Mn.CollectionView.extend({
  tagName: 'ul',
  className: 'menu',
  childView: Menu
});
