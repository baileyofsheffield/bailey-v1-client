var MobileMenuView = require('./views/show/mobile_menus'),
  AccountView = require('./views/show/account'),
  CartView = require('./views/show/cart'),
  MenusView = require('./views/show/menus'),
  MenuCollection = require('../../entities/menus'),
  MainMenu = require('../../config/menus/mainMenu'),
  userChannel = require('../../config/userChannel'),
  globalChannel = require('../../config/globalChannel'),
  main_menu,
  headerToSelect;

module.exports = Mn.Object.extend({
  account: function() {
    var self = this;
    var accountRegion = this.options.accountRegion;
    var user = userChannel.request('current:user');
    var accountView = new AccountView({
      model: user
    });
    self.listenTo(accountView, 'login:show', function() {
      globalChannel.trigger('login:show');
    });
    self.listenTo(accountView, 'user:show', function(model) {
      globalChannel.trigger('user:show', model.get('userid'));
    });
    accountRegion.show(accountView);
  },
  show: function() {
    var self = this;
    var user = userChannel.request('current:user');
    var auth = user.get('auth');
    var cartRegion = this.options.cartRegion;
    var menuRegion = this.options.menuRegion;
    var mobileMenuRegion = this.options.mobileMenuRegion;
    var desktopMenu = _.filter(MainMenu, function(menu) {
      return menu.desktop === true;
    });

    var mobileMenu = _.filter(MainMenu, function(menu) {
      if (menu.mobile === true || menu.auth === auth) {
        return menu;
      }
    });

    main_menu = new MenuCollection(desktopMenu);
    var mobile_menu = new MenuCollection(mobileMenu);
    var lineitems = new Backbone.Collection();
    var total = new Backbone.Model();
    var mobileView = new MobileMenuView({
      collection: mobile_menu
    });

    var menusView = new MenusView({
      collection: main_menu
    });

    var cartView = new CartView({
      collection: lineitems,
      total: total
    });

    self.listenTo(menusView, "childview:navigate", function(childView, navigationTrigger, slug) {
      globalChannel.trigger(navigationTrigger, slug);
    });

    self.listenTo(mobileView, "childview:navigate", function(childView, navigationTrigger, slug) {
      if (navigationTrigger == 'user:show') {
        globalChannel.trigger('user:show', user.get('userid'));
      } else {
        globalChannel.trigger(navigationTrigger, slug);
      }
    });

    self.listenTo(cartView, 'cart:show', function() {
      globalChannel.trigger('cart:show');
    });

    self.listenTo(globalChannel, 'basket:add', function(data, order) {
      total.set({total: order.total});
      lineitems.add(data, {merge: true});
      lineitems.trigger('sync');
    });

    self.listenTo(globalChannel, 'basket:remove', function(data, order) {
      total.set({total: order.total});
      lineitems.remove(data);
    });

    self.listenTo(globalChannel, 'basket:update', function(data, order) {
      total.set({total: order.total});
      var item = lineitems.get(data.id);
      item.set(data);
      lineitems.trigger('sync');
    });

    self.listenTo(globalChannel, 'basket:reset', function() {
      total.set({total: 0});
      lineitems.reset();
    });

    cartRegion.show(cartView);
    menuRegion.show(menusView);
    mobileMenuRegion.show(mobileView);
  },

  setActiveHeader: function(headerUrl) {
    if (headerToSelect) headerToSelect.deselect();
    headerToSelect = main_menu.find(function(header) {
      return header.get("url") === headerUrl;
    });
    if (headerToSelect) headerToSelect.select();
    main_menu.trigger("reset");
  }
});
