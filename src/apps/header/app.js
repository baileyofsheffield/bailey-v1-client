var app = require('../../app'),
  HeaderController = require('./controller'),
  userChannel = require('../../config/userChannel'),
  globalChannel = require('../../config/globalChannel'),
  HeaderModule;

HeaderModule = Mn.Module.extend({
  startWithParent: false,
  initialize: function() {
    return this.setHandlers();
  },
  onStart: function() {
    this.controller = new HeaderController({
      accountRegion: this.app.account,
      cartRegion: this.app.cart,
      menuRegion: this.app.menu,
      mobileMenuRegion: this.app.mobileMenu
    });
    this.controller.show();
    this.controller.account();
  },
  onStop: function() {
    this.controller.destroy();
  },
  setHandlers: function() {
    var _this = this;
    globalChannel.on('set:active:header', function(name) {
      return _this.controller.setActiveHeader(name);
    });
    userChannel.on('set:current:user', function(user) {
      _this.controller.show();
      return _this.controller.account();
    });
  }
});

module.exports = HeaderModule;
