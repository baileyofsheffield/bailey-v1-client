var tpl = require('./templates/loading.hbs');

module.exports = Mn.ItemView.extend({
  className: 'loading main-layout container',
  template: tpl
});
