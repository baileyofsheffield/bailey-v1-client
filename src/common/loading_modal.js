var tpl = require('./templates/loading_modal.hbs');

module.exports = Mn.ItemView.extend({
  className: 'modal-underlay',
  template: tpl
});
