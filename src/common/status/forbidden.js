var tpl = require('./templates/forbidden.hbs');

module.exports = Mn.ItemView.extend({
  tagName: 'section',
  className: 'main-wrapper error-page',
  template: tpl
});
