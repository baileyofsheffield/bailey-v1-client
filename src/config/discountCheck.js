var request = require('superagent');

exports.isAvailable = function() {
  return request.get("/api/public/discount/available/check");
};

exports.isValid = function(code, order) {
  var data = {
    order: order
  };
  return new Promise(function(resolve, reject) {
    request
      .get("/api/discount/verify/" + code.toUpperCase())
      .query(data)
      .end(function(err, res) {
        if (res) {
          if (res.ok) return resolve(res.body);
          if (res.notFound) return reject(res);
        }
        if (err) return reject(err);
      });
  });
};
