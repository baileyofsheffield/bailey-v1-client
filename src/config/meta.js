var app = require('../app');
module.exports = function(title, social_title, description, image) {
  document.title = title ? title + ' | Bailey of Sheffield' : 'Bailey of Sheffield';
  var host = location.protocol + '//' + location.host + '/';
  var social_image;
  if (image) {
    social_image = app.config.cloud + 'c_crop,w_1200,h_630' + '/' + image;
  }
  var image_path = social_image ? social_image : host + 'img/facebook-logo.jpg';

  $('meta[name="description"]').remove();
  if (description) {
    $('head').append('<meta name="description" content="' + description + '">');
  }

  $('meta[property="og:url"]').remove();
  $('head').append('<meta property="og:url" content="' + location.href + '">');

  $('meta[property="og:title"]').remove();
  $('head').append('<meta property="og:title" content="' + social_title + '">');

  $('meta[property="og:description"]').remove();
  if (description) {
    $('head').append('<meta property="og:description" content="' + description + '">');
  }

  $('meta[property="og:image"]').remove();
  $('head').append('<meta property="og:image" content="' + image_path + '">');
};
