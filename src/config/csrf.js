var request = require('superagent');

module.exports = function() {
  return request.get('/api/public/csrfToken').then(function(res) {
    var token = res.body;
    $.ajaxPrefilter(function(options, _, xhr) {
      xhr.setRequestHeader('X-CSRF-Token', token._csrf);
    });
    return token._csrf;
  });
};
