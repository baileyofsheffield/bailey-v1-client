var request = require('superagent'),
  userChannel = require('./userChannel');

module.exports = function(data) {
  return new Promise(function(resolve, reject) {
    request.get('/api/public/csrfToken').then(function(res) {
      var token = res.body;
      data._csrf = token._csrf;
      return request.post('/api/auth/local').send(data);
    }).then(function(res) {
      userChannel.trigger('set:current:user', res.body);
      resolve(res.body);
    }).catch(function(err) {
      Rollbar.error(err);
      reject(err);
    });
  });
};
