module.exports = Mn.AppRouter.extend({
  execute: function(callback, args) {
    var params = args[0],
      query = args[1] || '',
      options = {};
    _.defaults(options, {
      page: 1
    });
    if (params) {
      if (params.indexOf(":") < 0) {
        options = params;
      } else {
        if (params.trim() !== '') {
          params = params.split('+');
          _.each(params, function(param) {
            var values = param.split(':');
            if (values[1]) {
              if (values[0] === "page") {
                options[values[0]] = parseInt(values[1], 10);
              } else {
                options[values[0]] = values[1];
              }
            }
          });
        }
      }
    }
    if (callback) {
      callback.call(this, options, query);
    }
  }
});
