var request = require('superagent');

module.exports = function(items, product, engraving_options) {
  return request.get('/csrfToken').then(function(res) {
    var token = res.body;
    return request.post('/api/order/custom').send({
      items: items,
      product: product,
      _csrf: token._csrf,
      engraving_options: engraving_options
    });
  });
  // return new Promise(function(resolve, reject) {
  //   request.get('/csrfToken').then(function(res) {
  //     var token = res.body;
  //     return request.post('/api/order/custom').send({
  //       items: items,
  //       product: product,
  //       _csrf: token._csrf,
  //       engraving_options: engraving_options
  //     });
  //   }).then(function(resp) {
  //     resolve(resp);
  //   }).catch(function(err) {
  //     Rollbar.error(err);
  //     reject(err);
  //   });
  // });
};
