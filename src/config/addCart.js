var request = require('superagent');

module.exports = function(variation, product, engraving_options) {
  return request.get('/api/public/csrfToken').then(function(res) {
    var token = res.body;
    return request.post('/api/public/cart').send({
      variation: variation,
      product: product,
      engraving_options: engraving_options,
      _csrf: token._csrf
    });
  });
};
