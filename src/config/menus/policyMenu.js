module.exports = [{
  name: "Terms & Conditions",
  url: "policy/terms-and-conditions",
  slug: "terms-and-conditions",
  navigationTrigger: "page:show",
  category: 'policy'
}, {
  name: "Corporate Social Responsibility",
  url: "policy/csr",
  slug: "csr",
  navigationTrigger: "page:show",
  category: 'policy'
}, {
  name: "Cookie Policy",
  url: "policy/cookie-policy",
  slug: "cookie-policy",
  navigationTrigger: "page:show",
  category: 'policy'
}, {
  name: "Counterfeiting",
  url: "policy/counterfeiting",
  slug: "counterfeiting",
  navigationTrigger: "page:show",
  category: 'policy'
}];
