module.exports = [{
  name: "Delivery & Shipping",
  url: "essentials/delivery-and-shipping",
  slug: "delivery-and-shipping",
  navigationTrigger: "page:show",
  category: 'essentials'
}, {
  name: "Refunds & Exchanges",
  url: "essentials/refunds-and-exchanges",
  slug: "refunds-and-exchanges",
  navigationTrigger: "page:show",
  category: 'essentials'
}, {
  name: "Register Your Bailey Products",
  url: "essentials/register",
  slug: "register",
  navigationTrigger: "page:show",
  category: 'essentials'
}, {
  name: "Jewellery Care",
  url: "essentials/jewellery-care",
  slug: "jewellery-care",
  navigationTrigger: "page:show",
  category: 'essentials'
}, {
  name: "More Than Lifetime Guarantee",
  url: "essentials/more-than-lifetime-guarantee",
  slug: "more-than-lifetime-guarantee",
  navigationTrigger: "page:show",
  category: 'essentials'
}, {
  name: "10 Year Anniversary Pledge",
  url: "essentials/10-year-anniversary-pledge",
  slug: "10-year-anniversary-pledge",
  navigationTrigger: "page:show",
  category: 'essentials'
}, {
  name: "Bailey Bespoke",
  url: "essentials/bailey-bespoke",
  slug: "bailey-bespoke",
  navigationTrigger: "page:show",
  category: 'essentials'
}, {
  name: "Corporate Commissions",
  url: "essentials/corporate-commissions",
  slug: "corporate-commissions",
  navigationTrigger: "page:show",
  category: 'essentials'
}, {
  name: "Testimonials",
  url: "testimonials",
  slug: "corporate-commissions",
  navigationTrigger: "review:list",
  category: 'essentials'
}];
