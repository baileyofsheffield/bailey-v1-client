module.exports = [{
  name: "Orders",
  navigationTrigger: "user:order:list"
},{
  name: "Registered products",
  navigationTrigger: "user:product:register"
},{
  name: "Edit Profile",
  navigationTrigger: "user:edit"
},{
  name: "Log out",
  navigationTrigger: "user:logout"
}];
