module.exports = [{
  name: "Size Guide",
  url: "technical/size-guide",
  slug: "size-guide",
  navigationTrigger: "page:show",
  category: 'technical'
}, {
  name: "Metal Guide",
  url: "technical/metal-guide",
  slug: "metal-guide",
  navigationTrigger: "page:show",
  category: 'technical'
}, {
  name: "Diamond & Gemstone Guide",
  url: "technical/diamond-and-gemstone-guide",
  slug: "diamond-and-gemstone-guide",
  navigationTrigger: "page:show",
  category: 'technical'
}, {
  name: "PVD Guide",
  url: "technical/pvd-guide",
  slug: "pvd-guide",
  navigationTrigger: "page:show",
  category: 'technical'
}];
