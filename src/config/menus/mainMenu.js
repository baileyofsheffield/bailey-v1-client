module.exports = [{
  name: "Home",
  url: "",
  navigationTrigger: "home:show",
  desktop: true,
  mobile: true
},{
  name: "Shop",
  url: "shop",
  navigationTrigger: "shop:list",
  desktop: true,
  mobile: true
},{
  name: "Customise",
  url: "shop/custom",
  navigationTrigger: "shop:custom",
  desktop: true,
  mobile: true
}, {
  name: "Look Book",
  url: "look-book",
  navigationTrigger: "lookbook:list",
  desktop: true,
  mobile: true
}, {
  name: "Our Story",
  url: "story",
  navigationTrigger: "story:show",
  desktop: true,
  mobile: true
}, {
  name: "Workshop",
  url: "workshop",
  navigationTrigger: "workshop:show",
  desktop: true,
  mobile: true
}, {
  name: "Login/Register",
  url: "login",
  navigationTrigger: "login:show",
  class: "mobile",
  auth: false,
  desktop: false
}, {
  name: "Account",
  url: "user",
  navigationTrigger: "user:show",
  class: "mobile",
  auth: true,
  desktop: false
}];
