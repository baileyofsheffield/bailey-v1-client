var request = require('superagent');

module.exports = function(order) {
  return request.get('/api/public/csrfToken').then(function(res) {
    order._csrf = res.body._csrf;
    return request.post('/api/public/payment/new').send(order);
  });
};
