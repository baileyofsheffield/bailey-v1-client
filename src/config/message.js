module.exports = function(type, message, ttl) {
  var icon;
  switch (type) {
    case "error":
      icon = '<i class="icon fa fa-exclamation-triangle"></i>';
      break;
    case "notice":
      icon = '<i class="icon fa fa-check-circle"></i>';
      break;
  }
  var notification = new NotificationFx({
    message: '<h3>' + message + '</h3>',
    layout: 'bar',
    effect: 'exploader',
    ttl: ttl || 6000,
    type: type, // notice, warning or error
    onOpen: function() {
      setTimeout(function() {
        $('body').addClass('has-message');
      }, 500);
    },
    onClose: function() {
      setTimeout(function() {
        $('body').removeClass('has-message');
      }, 500);
    }
  });
  notification.show();
};
