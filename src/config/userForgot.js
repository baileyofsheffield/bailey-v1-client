var request = require('superagent'),
  userChannel = require('./userChannel');

module.exports = function(data) {
  return request.get('/api/public/csrfToken').then(function(res) {
    var token = res.body;
    data._csrf = token._csrf;
    return request.post('/api/auth/forgot').send(data);
  });
};
