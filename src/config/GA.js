var app = require('../app');
module.exports = {
	listView: function(products) {
		var variations = [];
		_.each(products, function(product, index) {
			_.each(product.variations, function(variation) {
				var data = {
					'id': variation.sku,
					'name': product.title,
					'category': product.product_type.title,
					'brand': 'Bailey of Sheffield',
					'variant': variation.size || variation.option_name,
					'list': 'Shop List',
					'position': index
				};
				variations.push(data);
			});
		});
		dataLayer.push({
			'ecommerce': {
				'impressions': variations
			},
			'event': 'trackImpressions'
		});
	},
	detailView: function(product, dimension) {
		var products = [];
		_.each(product.variations, function(variation) {
			var data = {
				'id': variation.sku,
				'name': product.title,
				'category': product.product_type.title,
				'variant': variation.size || variation.option_name,
				'brand': 'Bailey of Sheffield',
				'dimension1': dimension
			};
			products.push(data);
		});
		var product_title, product_price;
		if (product.parent_sku == 'CUSTOM') {
			product_price = ((product.variations[0].price * 4) / 100).toFixed(2);
		} else {
			product_price = (product.variations[0].price / 100).toFixed(2);
		}
		if (product.product_type.title == "Cable™" || product.product_type.title == "Bead") {
			product_title = product.title + ' ' + product.product_type.title;
		} else {
			product_title = product.title;
		}
		dataLayer.push({
			'schema_sku': product.parent_sku,
			'schema_title': product_title,
			'schema_description': product.fields[0].seo_description,
			'schema_category': product.product_type.title,
			'schema_image': app.config.cloud + 'w_600' + '/' + product.listing_image.public_id,
			'ecomm_totalvalue': product_price,
			'ecomm_prodid': product.parent_sku,
			'ecommerce': {
				'detail': {
					'products': products
				}
			},
			'event': 'trackProductView'
		});
	},
	addCart: function(product, line_item) {
		var products = [];
		if (line_item.custom_variations && line_item.custom_variations.length > 0) {
			_.each(line_item.custom_variations, function(variation) {
				var data = {
					'id': variation.product.sku,
					'name': product.title,
					'category': 'Custom',
					'variant': variation.product.size || variation.product.option_name,
					'brand': 'Bailey of Sheffield',
					'price': (variation.product.price / 100).toFixed(2),
					'quantity': line_item.qty
				};
				products.push(data);
			});
		} else {
			var data = {
				'id': line_item.product.sku,
				'name': product.title,
				'category': product.product_type.title,
				'variant': line_item.product.size || line_item.product.option_name,
				'brand': 'Bailey of Sheffield',
				'price': (line_item.product.price / 100).toFixed(2),
				'quantity': line_item.qty
			};
			products.push(data);
		}
		dataLayer.push({
      'ecomm_totalvalue': (line_item.total / 100).toFixed(2),
      'ecomm_prodid': product.parent_sku,
			'product_ids': _.pluck(products, 'id'),
			'event': 'addToCart',
			'ecommerce': {
				'add': {
					'products': products
				}
			}
		});
	},
	removeCart: function(line_item) {
		var products = [];
		if (line_item.custom_variations && line_item.custom_variations.length > 0) {
			_.each(line_item.custom_variations, function(variation) {
				var data = {
					'id': variation.product.sku,
					'name': variation.product.title,
					'category': 'Custom',
					'variant': variation.product.size || variation.product.option_name,
					'brand': 'Bailey of Sheffield',
					'price': (variation.product.price / 100).toFixed(2),
					'quantity': line_item.qty
				};
				products.push(data);
			});
		} else {
			var data = {
				'id': line_item.product.sku,
				'name': line_item.product.title,
				'category': line_item.product_category,
				'variant': line_item.product.size || line_item.product.option_name,
				'brand': 'Bailey of Sheffield',
				'price': (line_item.product.price / 100).toFixed(2),
				'quantity': line_item.qty
			};
			products.push(data);
		}
		dataLayer.push({
			'event': 'removeFromCart',
			'ecommerce': {
				'remove': {
					'products': products
				}
			}
		});
	},
	checkoutStep: function(step, items) {
		var products = [];
		items.each(function(item, index) {
			var line_item = item.toJSON();
			if (line_item.custom_variations && line_item.custom_variations.length > 0) {
				_.each(line_item.custom_variations, function(variation) {
					var data = {
						'id': variation.product.sku,
						'name': variation.product.title,
						'category': 'Custom',
						'variant': variation.product.size || variation.product.option_name,
						'brand': 'Bailey of Sheffield',
						'price': (variation.product.price / 100).toFixed(2),
						'quantity': line_item.qty
					};
					products.push(data);
				});
			} else {
				var data = {
					'id': line_item.product.sku,
					'name': line_item.product.title,
					'category': line_item.product_category,
					'variant': line_item.product.size || line_item.product.option_name,
					'brand': 'Bailey of Sheffield',
					'price': (line_item.product.price / 100).toFixed(2),
					'quantity': line_item.qty
				};
				products.push(data);
			}
		});
		if (step == 2) {
			dataLayer.push({
				'event': 'fb_checkout',
				'product_ids': _.pluck(products, 'id'),
				'fb_track_type': 'InitiateCheckout',
			});
		}
		if (step == 3) {
			dataLayer.push({
				'event': 'fb_checkout',
				'product_ids': _.pluck(products, 'id'),
				'fb_track_type': 'AddPaymentInfo',
			});
		}
		dataLayer.push({
			'event': 'checkout',
			'ecommerce': {
				'checkout': {
					'actionField': {
						'step': step
					},
					'products': products
				}
			}
		});
	},
	transaction: function(order, items) {
		var products = [];
		_.each(items, function(line_item) {
			if (line_item.custom_variations && line_item.custom_variations.length > 0) {
				_.each(line_item.custom_variations, function(variation) {
					var data = {
						'id': variation.product.sku,
						'name': variation.product.title,
						'category': 'Custom',
						'variant': variation.product.size || variation.product.option_name,
						'brand': 'Bailey of Sheffield',
						'price': (variation.product.price / 100).toFixed(2),
						'quantity': line_item.qty
					};
					products.push(data);
				});
			} else {
				var data = {
					'id': line_item.product.sku,
					'name': line_item.product.title,
					'category': line_item.product_category,
					'variant': line_item.product.size || line_item.product.option_name,
					'brand': 'Bailey of Sheffield',
					'price': (line_item.product.price / 100).toFixed(2),
					'quantity': line_item.qty
				};
				products.push(data);
			}
      dataLayer.push({
        'event': 'trackTransactionAd',
        'ecomm_totalvalue': (line_item.total / 100).toFixed(2),
        'ecomm_prodid': line_item.product_display.parent_sku
      });
		});
		var order_data = {
			'id': order.orderid,
			'revenue': (order.total / 100).toFixed(2),
			'shipping': (order.shipping_price / 100).toFixed(2)
		};
		if (order.discount_code) {
			order_data.coupon = order.discount_code;
		}
		dataLayer.push({
			'event': 'trackTransaction',
			'orderid':  order.orderid,
			'product_ids': _.pluck(products, 'id'),
			'order_total': (order.total / 100).toFixed(2),
			'ecommerce': {
				'purchase': {
					'actionField': order_data,
					'products': products
				}
			}
		});
	}
};
