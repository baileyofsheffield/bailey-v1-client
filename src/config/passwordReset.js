var request = require('superagent');

module.exports = function(data) {
  return request.get('/csrfToken').then(function(res) {
    var token = res.body;
    data._csrf = token._csrf;
    return request.post('/api/reset').send(data);
  });
};
