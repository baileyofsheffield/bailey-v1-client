module.exports = Mn.Region.Modal = Mn.Region.extend({

  constructor: function() {
    _.bindAll(this, 'showModal', 'hideModal');
    Backbone.Marionette.Region.prototype.constructor.apply(this, arguments);
    this.listenTo(this, "show", this.showModal, this);
  },

  showModal: function(view) {
    this.view = view;
    this.listenTo(view, "modal:close", this.hideModal);
    $('#modal').addClass('show');
    $('body').addClass('modal-open');
  },

  hideModal: function(view) {
    if (this.view) {
      this.view.destroy();
    }
    $('#modal').removeClass('show');
    $('body').removeClass('modal-open');
  }
});
