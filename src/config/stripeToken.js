module.exports = function(billing, cc_number, cc_exp, cc_cvc) {
  return new Promise(function(resolve, reject) {
    var data = {
      number: cc_number,
      cvc: cc_cvc,
      exp: cc_exp
    };
    if (billing) {
      data = {
        number: cc_number,
        cvc: cc_cvc,
        exp: cc_exp,
        address_line1: billing.address1,
        address_line2: billing.address2 || '',
        address_city: billing.city,
        address_state: billing.region,
        address_zip: billing.postcode || '',
        address_country: billing.country_code
      };
    }
    Stripe.card.createToken(data, function (status, response) {
      if (response.error) {
        Rollbar.error(response.error);
        Rollbar.info("Billing Info", data);
        reject(response.error);
      } else {
        resolve(response);
      }
    });
  });
};
