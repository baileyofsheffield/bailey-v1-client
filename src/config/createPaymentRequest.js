var request = require('superagent');

module.exports = function(data) {
  return request.get('/api/public/csrfToken').then(function(res) {
    data._csrf = res.body._csrf;
    return request.post('/api/paymentrequest/pay').send(data);
  });
};
