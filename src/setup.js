require('es6-promise').polyfill();
var rollbar = require('rollbar-browser');
var rollbarConfig = {
  accessToken: '80d1d3acaba744ae9dec30e4955a7e54',
  captureUncaught: true,
  payload: {
    environment: 'production',
    client: {
      javascript: {
        source_map_enabled: true,
        code_version: '1.2.0',
        guess_uncaught_frames: true
      }
    }
  }
};
var Rollbar = rollbar.init(rollbarConfig);
window.Rollbar = Rollbar;
