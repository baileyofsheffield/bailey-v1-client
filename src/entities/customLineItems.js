var CustomLineItem = require('./customLineItem.js');
Backbone.LocalStorage = require("backbone.localstorage");
module.exports = Backbone.Collection.extend({
  localStorage: new Backbone.LocalStorage("customlineitem"),
  model: CustomLineItem
});
