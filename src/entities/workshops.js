var Backbone = require('backbone'),
  Workshop = require('./workshop.js');

module.exports = Backbone.Collection.extend({
  url: "/api/workshop",
  model: Workshop
});
