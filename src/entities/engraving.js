module.exports = Backbone.Model.extend({
    urlRoot: "/api/public/engraving",
    idAttribute: '_id',
    defaults: {
        text: '',
        editorOpen: true,
        editable: false,
        total: 0
    }
});
