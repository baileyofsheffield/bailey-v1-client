module.exports = Backbone.Model.extend({
  urlRoot: "/api/public/auth/reset",
  initialize: function() {
    var that = this;
    $.ajaxPrefilter(function(options, originalOptions, jqXHR) {
      options.xhrFields = {
        withCredentials: true
      };
    });
  },
  validation: {
    password: {
      required: true,
      minLength: 8,
      msg: 'Please enter a valid password'
    },
    confirm: {
      required: true,
      equalTo: 'password',
      msg: 'Passwords don\'t match'
    }
  }
});
