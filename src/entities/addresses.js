var Address = require('./address.js');

module.exports = Backbone.Collection.extend({
  url: "/api/public/customeraddress",
  model: Address
});
