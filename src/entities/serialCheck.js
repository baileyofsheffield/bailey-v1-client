var Backbone = require('backbone');

module.exports = Backbone.Model.extend({
  urlRoot: "/api/productserial/check",
  validation: {
    serial_number: {
      required: true,
      msg: 'Please enter a valid serial number'
    }
  }
});
