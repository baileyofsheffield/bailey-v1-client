Backbone.LocalStorage = require("backbone.localstorage");
module.exports = Backbone.Model.extend({
  localStorage: new Backbone.LocalStorage("customlineitem")
});
