var userChannel = require('../config/userChannel');

var userModel = new Backbone.Model();

userChannel.reply('current:user', function() {
  return userModel;
});

userChannel.on('set:current:user', function(user) {
  userModel.set(user);
  return userModel;
});

userChannel.on('set:guest:user', function(user) {
  userModel.set({guest: true});
  return userModel;
});
