var LookBook = require('./lookbook.js');

module.exports = Backbone.Collection.extend({
  url: "/api/public/lookbook",
  model: LookBook
});
