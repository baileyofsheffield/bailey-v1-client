var ProductDisplay = require('./productDisplay.js');

module.exports = ProductDisplayCollection = Backbone.Collection.extend({
    url: "/api/public/productdisplay",
    model: ProductDisplay,
    parse: function(resp) {
        return resp.docs;
    }
});
