module.exports = Backbone.Model.extend({
    urlRoot: "/api/customoption",
    idAttribute: '_id',
    validation: {
        name: {
            required: true
        }
    }
});
