module.exports = Backbone.Model.extend({
  urlRoot: "/api/public/page",
  idAttribute: "slug"
});
