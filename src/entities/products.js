var Product = require('./product.js');

module.exports = Backbone.Collection.extend({
  url: "/api/product",
  model: Product
});
