require('backbone.select');

module.exports = Backbone.Model.extend({
  initialize: function() {
    Backbone.Select.Me.applyTo( this );
  }
});
