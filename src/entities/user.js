var Backbone = require('backbone');

module.exports = Backbone.Model.extend({
  urlRoot: "/api/user/account",
  idAttribute: 'userid',
  validation: {
    email: {
      required: true,
      pattern: 'email',
      msg: 'Please enter a valid email'
    },
    confirm_email: {
      required: function(value, attr, computedState) {
        if (computedState.update) {
          return false;
        }
        return true;
      },
      equalTo: 'email',
      msg: 'Email addresses don\'t match'
    },
    password: {
      required: function(value, attr, computedState) {
        if (computedState.update) {
          return false;
        }
        return true;
      },
      minLength: 8,
      msg: 'Please enter a valid password'
    },
    confirm: {
      required: function(value, attr, computedState) {
        if (computedState.update) {
          return false;
        }
        return true;
      },
      equalTo: 'password',
      msg: 'Passwords don\'t match'
    }
  },
  signup: function(attrs, options) {
    this.urlRoot = '/api/public/auth/signup';
    return this.save(attrs);
  }
});
