module.exports = Backbone.Model.extend({
  urlRoot: "/api/public/productdisplay",
  idAttribute: 'slug'
});
