var Sidebar = require('./menu.js');

module.exports = Backbone.Collection.extend({
  model: Sidebar,
  initialize: function(models, options) {
    Backbone.Select.One.applyTo(this, models, options);
  }
});
