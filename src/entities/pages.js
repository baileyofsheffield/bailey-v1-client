var PageableCollection = require("backbone.paginator"),
  Page = require('./page.js');

module.exports = PageCollection = Backbone.PageableCollection.extend({
  url: "/api/public/page",
  model: Page,
  initialize: function(models, options) {
    if (!options) options = {};
    var params = options.parameters || {
      page: 1
    };
    this.parameters = new Backbone.Model(params);
    this.state = {
      firstPage: 1,
      currentPage: this.parameters.get('page'),
      perPage: 17
    };

    if (this.parameters.get("keyword")) {
      this.url = '/api/page/search';
    }

    this.queryParams = {
      limit: function() {
        return this.state.perPage;
      },
      skip: function() {
        return ((this.parameters.get("page") || 1) - 1) * this.state.perPage;
      },
      search: function() {
        return this.parameters.get("keyword");
      },
      populate: function() {
        return ['inventory'];
      }
    };

    var self = this;
    this.listenTo(this.parameters, "change", function(model) {
      if (self.parameters.get("keyword")) {
        self.url = '/api/page/search';
      } else {
        self.url = '/api/page';
      }
      if (_.has(model.changed, "keyword")) {
        self.queryParams.search = self.parameters.get("keyword");
      }
      self.getPage(model.get('page')).done(function() {
        self.trigger("page:change:after");
      });
    });
  }
});

_.extend(PageCollection.prototype, {
  parse: function(resp) {
    this.totalPages = Math.ceil(resp.total / this.state.perPage);
    this.currentPage = this.parameters.get("page");
    return resp.docs;
  },
  fetchAll: function() {
    return this.fetch();
  },
  checkSku: function(sku) {
    this.url = '/api/page/sku';
    return this.fetch({data: {sku: sku}});
  }
});
