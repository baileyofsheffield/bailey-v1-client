module.exports = Backbone.Model.extend({
  urlRoot: "/api/public/cart/order",
  idAttribute: 'orderid',
  validation: {
    'email': {
      required: true,
      pattern: 'email'
    }
  },
  fetchAccount: function (options) {
    this.urlRoot = '/api/order/account';
    return this.fetch(options);
  },
  updateSurvey: function (attrs, options) {
    this.urlRoot = '/api/order/survey';
    return this.save(attrs);
  }
});
