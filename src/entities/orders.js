var Order = require('./order.js');

module.exports = Backbone.Collection.extend({
  url: "/api/order",
  model: Order,
  accountOrders: function (options) {
    this.url = '/api/order/account';
    return this.fetch(options);
  }
});
