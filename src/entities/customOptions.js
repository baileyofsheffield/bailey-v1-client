var CustomOption = require('./customOption.js');

module.exports = Backbone.Collection.extend({
  url: "/api/customoption",
  model: CustomOption
});
