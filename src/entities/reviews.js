var Review = require('./review.js');

module.exports = Backbone.Collection.extend({
	url: "/api/public/review",
	model: Review
});
