module.exports = Backbone.Model.extend({
  urlRoot: "/api/public/auth/local",
  initialize: function() {
    var that = this;
    $.ajaxPrefilter(function(options, originalOptions, jqXHR) {
      options.xhrFields = {
        withCredentials: true
      };
    });
  },
  validation: {
    email: {
      required: true,
      pattern: 'email',
      msg: 'Please enter a valid email'
    },
    password: {
      required: true,
      msg: 'Please enter your password'
    }
  }
});
