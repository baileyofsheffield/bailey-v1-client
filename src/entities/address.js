var userChannel = require('../config/userChannel');

module.exports = Backbone.Model.extend({
  urlRoot: "/api/public/customeraddress",
  idAttribute: '_id',
  saveOrder: function(attrs, options) {
    this.urlRoot = '/api/public/customeraddress/order';
    return this.save(attrs);
  },
  validation: {
    'first_name': {
      required: true,
      msg: 'Please enter your first name'
    },
    'last_name': {
      required: true,
      msg: 'Please enter your last name'
    },
    'address1': {
      required: true,
      msg: 'Please enter your last name'
    },
    'city': {
      required: true,
      msg: 'Please enter your city'
    },
    'postcode': {
      required: function(val, attr, computed) {
        if (computed.country == 'Ireland') {
          return false;
        } else {
          return true;
        }
      },
      msg: 'Please enter your postcode'
    },
    'phone': {
      required: function(value, attr, computedState) {
        if (computedState.type == 'Billing') {
          return false;
        } else {
          return true;
        }
      },
      msg: 'Please enter your phone number'
    },
    'email': {
      required: function(value, attr, computedState) {
        if (computedState.type == 'Billing') {
          return false;
        }
        var user = userChannel.request('current:user');
        var auth = user.get('auth');
        return !auth;
      },
      pattern: 'email',
      msg: 'Please enter a valid email address'
    }
  }
});
