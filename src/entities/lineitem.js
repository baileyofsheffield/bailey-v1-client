module.exports = Backbone.Model.extend({
    urlRoot: "/api/public/cart",
    idAttribute: '_id',
});
