var Serial = require('./serial.js');

module.exports = Backbone.Collection.extend({
  url: "/api/productserial/customer",
  model: Serial
});
