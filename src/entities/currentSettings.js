var globalChannel = require('../config/globalChannel');

var settingsModel = new Backbone.Model();

globalChannel.reply('current:settings', function() {
  return settingsModel;
});

globalChannel.on('set:current:settings', function(settings) {
  settingsModel.set(settings);
  return settingsModel;
});
