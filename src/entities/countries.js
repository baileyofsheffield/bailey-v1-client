var Country = require('./country.js');

module.exports = Backbone.Collection.extend({
  url: "/api/public/country",
  model: Country
});
