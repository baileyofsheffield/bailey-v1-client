module.exports = Backbone.Model.extend({
  urlRoot: "/api/workshop",
  idAttribute: "slug"
});
