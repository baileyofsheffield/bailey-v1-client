module.exports = Backbone.Model.extend({
  urlRoot: "/api/product",
  idAttribute: "slug"
});
