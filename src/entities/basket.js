var globalChannel = require('../config/globalChannel');

var basketCollection = new Backbone.Collection();

globalChannel.reply('current:basket', function() {
  return basketCollection;
});

globalChannel.on('set:current:basket', function(items) {
  basketCollection.add(items);
  return basketCollection;
});

globalChannel.on('basket:add', function(item) {
  basketCollection.add(item);
  return basketCollection;
});

globalChannel.on('basket:remove', function(item) {
  basketCollection.remove(item);
  return basketCollection;
});
