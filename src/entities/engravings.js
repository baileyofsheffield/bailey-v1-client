var Engraving = require('./engraving.js');

module.exports = Backbone.Collection.extend({
	url: "/api/public/engraving",
	model: Engraving,
	getTotal: function() {
    return this.reduce(function(memo, value) {
      var obj = value.toJSON();
      return memo + obj.total;
    }, 0);
  },
	getAdded: function() {
    return this.where({added: true});
  }
});
