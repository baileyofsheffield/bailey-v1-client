module.exports = Backbone.Model.extend({
    urlRoot: "/api/public/lookbook",
    idAttribute: '_id'
});
