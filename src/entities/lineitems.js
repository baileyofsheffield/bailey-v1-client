var LineItem = require('./lineitem.js');

module.exports = Backbone.Collection.extend({
  url: "/api/public/cart",
  model: LineItem,
  cartTotal: function() {
    return this.reduce(function(memo, value) {
      return memo + value.get("total");
    }, 0);
  },
  cartQty: function() {
    return this.reduce(function(memo, value) {
      return memo + value.get("qty");
    }, 0);
  },
  orderItems: function (options) {
    this.url = '/api/lineitem/order';
    return this.fetch(options);
  }
});
