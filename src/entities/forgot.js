module.exports = Backbone.Model.extend({
  urlRoot: "/api/public/auth/forgot",
  initialize: function() {
    $.ajaxPrefilter(function(options, originalOptions, jqXHR) {
      options.xhrFields = {
        withCredentials: true
      };
    });
  },
  validation: {
    email: {
      required: true,
      pattern: 'email',
      msg: 'Please enter a valid email'
    }
  }
});
